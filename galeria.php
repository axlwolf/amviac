<?php

include "includes/funciones.php";
$titulo = "Galería";
include ('includes/header3.php');

//obtener testos sider
$query = "SELECT * FROM empresa";

//#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);

while ($texto = $resultado -> fetch_assoc()) {
	$empresa1 = $texto['empresa'];
	$somos = $texto['somos'];
	$direccion = $texto['direccion'];
	$filosofia = $texto['filosofia'];
	$eslogan = $texto['eslogan'];
}
?>

<section id="ccr-left-section" class="col-md-12">
	<div class="current-page">
		<a href="index.php"><i class="fa fa-home"></i> <i class="fa fa-angle-double-right"></i></a> <?= $titulo ?>
	</div>
	<!-- / .current-page -->

	<!-- BreadCrumb -->
	<div class="body-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="margin-bottom: 300px">
					<div class="portfolio-filter-container">
						<div class="portfolio-filter-label">
							Filtrar por :
						</div>
						<ul class="portfolio-filter">
                            <li>
                                <a href="#" class="portfolio-selected" data-filter="*">Todas las categorías</a>
                            </li>
                            <?php
//                            print_r($datos);
                             foreach($datos as $categoria):

                            ?>

                                 <?php

                                 ?>
                                 <li>
                                     <a href="#" data-filter=".<?= $categoria['categoria'] ?>"><?= $categoria['categoria'] ?></a>
                                 </li>
                             <?php
                             endforeach;
                            ?>
                          </ul>
					</div>

					<div class="portfolio-items">
                        <?php
//                        $datos2[] = array(
//                            'nombre' 			   => $row['nombre'],
//                            'titulo' 			   => $row['titulo'],
//                            'id_galeria' 		   => $row['id_galeria'],
//                            'galeria.categoria'    => $row['galeria.categoria']
//                        );
//                                                        print_r($datos2);

                        foreach($datos2 as $foto):
                        ?>


                            <div class="thumb-label-item <?= $foto['categoria'] ?>">
                                <div class="img-overlay thumb-label-item-img">
                                    <img
                                        src="galeria/<?= $foto['nombre'] ?>.png"
                                        alt=""/>

                                    <div class="item-img-overlay">
                                        <div class="item_img_overlay_content">

                                            <a href="galeria/<?= $foto['nombre'] ?>.png" data-rel="prettyPhoto[portfolio1]" title="Título"> <i class="fa fa-search"></i>
                                            </a>

                                        </div>

                                    </div>
                                </div>

                                <h3 class="thumb-label-item-title"><a href="#">  <p>
                                            <?= $foto['titulo'] ?>

                                        </p> </a></h3>

                                <div class="thumb-label-title-tags">
                                    <!--		<a href="#">Design</a>-->
                                    <!--		, <a href="#">Videos</a>-->
                                    <!--		, <a href="#">Logos</a>-->
                                </div>

                            </div>

                            <?php
                            endforeach;
                            ?>

					</div>
				</div>
			</div>
		</div>
	</div>

</section>


<?php
if ($titulo == "Inicio") {
	include "includes/footer3.php";
} else {
	include "includes/footer2.php";
}
?>