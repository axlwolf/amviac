<?php
include ('admin/php_conexion.php');
include ('includes/header.php');
?>

<section class="row">
	<h1> Aviso de Privacidad </h1>
	<p>
		AVISO DE PRIVACIDAD
		1.- UNIVERSIDAD DEL VALLE DE MÉXICO, S.C.; Y/O UVM  (la “Institución”) con domicilio en Campos Elíseos, número 223, Colonia Chapultepec Polanco, Delegación Miguel Hidalgo, C.P. 11560, México, Distrito Federal, es responsable de recabar sus datos personales, del uso que se le dé a los mismos y de su protección. Teléfono 91385000.

		Los datos personales recabados en el presente formato, tienen como finalidadque los datos personales y/o sensibles, en su caso, recabados por virtud del presente formato,  puedan ser utilizados para la debida operación de la Institución, incluyendo su transmisión a otras Instituciones Educativas, Agencias Calificadoras y autoridades competentes que tengan necesidad de conocerlos para el debido cumplimiento de los fines de esta Institución, con el objeto de que los mismos sean utilizados para efectos académicos, administrativos, de mercadotecnia, ventas y estadísticos, incluyendo de manera enunciativa más no limitativa, la validación de la autenticidad de certificados, diplomas, títulos o cualquier otro documento que sea expedido a favor del alumno, ex-alumno, o posible alumno; la confirmación de la autenticidad de la documentación que Ud. entregue; la entrega de dichos datos a cualquier autoridad competente que lo solicite; la realización de cualquier trámite interno de la Institución que sea necesario para poder cumplir los fines de la misma; el envío de información por parte de la Institución que le sea necesaria o conveniente para cursar y terminar sus estudios; su difusión en los diversos programas de prácticas profesionales y servicio social; invitación a ciertos eventos de la Institución, la cobranza de adeudos pendientes; la evaluación de la calidad del servicio de la Institución, y en general, para cualquier fin académico o administrativo que sea requerido o que tenga relación con la condición de alumno, ex-alumno, o posible alumno de la Institución, para lo cual requerimos los siguientes datos:

		Nombre.
		Estado Civil.
		Fecha de nacimiento.
		Correo electrónico.
		Teléfono de casa, oficina y celular, en su caso.
		Dirección.
		Último nivel de estudios y nombre de la Institución.
		Lugar de trabajo y sus datos, así como si el negocio es propio y fecha de antigüedad.
		Programa escolar de interés.
		Ciclo escolar de interés.
		La forma en que se enteró de los programas escolares de la Institución.
		Cualquier otro dato que sea necesario para la debida prestación de los servicios de la Institución.
		En caso de no contar con dicha información la Institución se vería imposibilitada para inscribir y realizar cualquier trámite relacionado con el estudiante, posible estudiante o ex estudiante.
		Para las finalidades indicadas en el presente aviso de privacidad, la Institución puede recabar sus datos personales de la siguiente manera:

		Proporcionados directamente por el estudiante o posible estudiante.
		Obtenidas a través de otras fuentes permitidas por Ley.
		Por visitas al sitio de Internet de la Institución o el uso de sus servicios en línea.
		Cabe mencionar, que al accesar sitios de internet, se podrán encontrar con “cookies” que  son archivos de texto que son descargados automáticamente y almacenados en el disco duro del equipo de cómputo del usuario al navegar la página de internet específica, los cuales  permiten grabar en el servidor de Internet algunos de sus datos.

		Asimismo, las páginas de Internet pueden contener web beacons, que son imágenes insertadas en la página o correo electrónico, que pueden ser utilizadas para monitorear el comportamiento de un visitante, así como para almacenar información sobre la dirección IP del usuario, duración del tiempo de interacción de dicha página y el tipo de navegador utilizado, entre otros.

		En virtud de lo anterior, le informamos que la Institución pudiera utilizar “cookies” y “web beacons” para un mejor desempeño del sitio.
		Estas “cookies” y otras tecnologías pueden ser deshabilitadas en las opciones de configuración del navegador que se esté usando.

		Por otro lado, le informamos que usted tiene derecho de acceder a sus datos personales y/o sensibles, en su caso, en nuestro poder y a los detalles del tratamiento de los mismos, así como a rectificarlos en caso de ser inexactos, o instruir cancelarlos cuando considere que resulten ser excesivos o innecesarios para las finalidades que justificaron su obtención y/u oponerse al tratamiento de los mismos para ciertos fines específicos.

		Los mecanismos que se han implementado para el ejercicio de dichos derechos, son a través de la presentación de la solicitud respectiva en la Dirección de Servicios Escolares o Dirección de Mercadotecnia, según sea el caso, del campus al cual pertenece, en el cual presentó su solicitud o del campus en el cual cursó sus estudios.

		Sus datos personales pueden ser transferidos y tratados a terceros dentro y fuera del país, con personas distintas a la Institución, por lo que su información puede ser compartida con asesores, calificadores y reguladores de la Institución, así como a otras Instituciones Educativas.

		Esta declaración de Confidencialidad / Privacidad está sujeta a los términos y condiciones de todos los sitios web de la institución antes descritos, lo cual constituye un acuerdo legal entre el usuario y la institución. Si el usuario utiliza los servicios en cualquiera de los sitios de la institución, significa que ha leído, entendido y acordado los términos antes expuestos. Si no está de acuerdo con ellos, el usuario no deberá proporcionar ninguna información personal, ni utilizar los servicios de los sitios de la institución.

		Nos reservamos el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente aviso de privacidad, para la atención de novedades legislativas, jurisprudenciales, políticas internas, nuevos requerimientos solicitados por la Institución, entre otras.

		Estas modificaciones estarán disponibles al público a través de los siguientes medios: la página de internet de la Institución www.uvmmexico.mx  , el Departamento de servicios escolares y el Departamento de Mercadotecnia de cada campus.

		En nuestro programa de notificación de promociones, ofertas y servicios a través de correo electrónico, sólo la Institución tiene acceso a la información recabada. Este tipo de publicidad se realiza mediante avisos y mensajes promocionales de correo electrónico, los cuales sólo serán enviados a los usuarios y a aquellos contactos registrados, esta indicación podrá usted modificarla en cualquier momento en los correos electrónicos enviados, en ellos pueden incluirse ocasionalmente ofertas de terceras partes que sean nuestros socios comerciales.
		En el caso de empleo de cookies, el botón de “ayuda” que se encuentra en la barra de herramientas de la mayoría de los navegadores, le dirá cómo evitar aceptar nuevos cookies, cómo hacer que el navegador le notifique cuando recibe un nuevo cookie o cómo deshabilitar todos los cookies.
		La seguridad y la confidencialidad de los datos que los usuarios proporcionen al contratar un servicio o comprar un producto en línea estarán protegidos por un servidor seguro bajo el protocolo Secure Socket Layer (SSL), de tal forma que los datos enviados se transmitirán encriptados para asegurar su resguardo.
		Para verificar que se encuentra en un entorno protegido asegúrese de que aparezca una S en la barra de navegación. Ejemplo: httpS://.
		Sin embargo, y a pesar de contar cada día con herramientas más seguras, la protección de los datos enviados a través de Internet no se puede garantizar al 100%; por lo que una vez recibidos, se hará todo lo posible por salvaguardar la información.

		Hacemos de su conocimiento que para cumplir con las finalidades previstas en este aviso y la Ley aplicable, serán recabados y tratados datos personales sensibles, los cuales se relacionan a su esfera más íntima, tales como aspectos de origen racial o étnico, estado de salud, creencias religiosas, filosóficas y morales, opiniones políticas, entre otras.
		Nos comprometemos a que los mismos serán tratados bajo medidas de seguridad y garantizando su confidencialidad.
		En todo momento usted podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales y/o sensibles, en su caso, a fin de que dejemos de hacer uso de los mismos.

		Para ello, es necesario que presente su petición por escrito mediante el siguiente procedimiento:

		Deberá ser necesario presentar la solicitud correspondiente por escrito en la Dirección de Servicios Escolares o de Mercadotecnia, según sea el caso, del campus a que pertenece, en el cual haya presentado su solicitud de admisión o en aquél en el cual cursó sus estudios, y en la que deberá incluir su  nombre completo y en el caso de alumnos, la clave correspondiente, haciendo la mención específica de que es su voluntad revocar el consentimiento, así como los alcances y causas de dicha revocación.

		A dicha solicitud se dará respuesta en un término no mayor a treinta días hábiles, en la que se le informará las medidas que se han realizado para realizar la revocación solicitada, la cual deberá ser recogida por la persona en la dirección de servicios escolares.

		En caso de que desee accesar, rectificar, cancelar u oponerte a ciertos datos personales que conforman la base de datos de la Institución, deberá cumplir con los siguientes requisitos:

		Enviar una solicitud de acceso, rectificación, cancelación u oposición de Información al Departamento de Servicios Escolares o de Mercadotecnia, según sea el caso.
		Acompañar los documentos que acrediten su identidad Una descripción clara y precisa del deseo de accesar, rectificar, cancelar u oponerse a algún dato personal contenido en la base de datos; acompañando los documentos que sustenten el motivo de la rectificación, cancelación u oposición al dato personal.
		Acompañar cualquier otro documento que facilite la localización de sus datos personales.
		Estimado usuario, se le notifica que el Aviso de Privacidad ha sido modificado el día 20 de octubre de 2011
		UNIVERSIDAD DEL VALLE DE MÉXICO, S.C.; Y/O UVM (la “Institución”) con domicilio en Edificio Samara, Avenida Santa Fe, No. 94, Torre D, Piso 13, Colonia Zedec Santa Fe, C.P. 01210, Delegación Álvaro Obregón, México, Distrito Federal, es responsable de recabar sus datos personales, del uso que se le dé a los mismos y de su protección. Teléfono 91385000.

		Los datos personales (los datos) divulgados esta Institución, tienen como finalidad que los datos personales y/o datos sensibles, que en su caso sean recabados por virtud del presente Aviso de Privacidad, puedan ser utilizados para la debida operación y fines de la Institución (y todas sus áreas), incluyendo su transmisión dentro y fuera del país a: filiales y subsidiarias de Laureate Education, Inc., instituciones educativas, socios comerciales u otros terceros y a las autoridades gubernamentales que así lo requieran; lo anterior con el objeto de que los mismos puedan ser utilizados para efectos académicos, administrativos, comerciales, mercadológicos, ventas, estadísticos y otros que puedan ser de interés y/o en beneficio del Titular.

		Con base en lo anterior, usted, al proporcionar sus datos como Titular de los mismos, otorga su consentimiento a la Institución para que ésta pueda utilizarlos para los fines aquí descritos; asimismo, queda al tanto de los derechos que usted ostenta como Titular de los datos.

		Por otro lado, e independientemente de los datos que puedan ser recabados del público en general, para el caso de ex-alumnos, alumnos o prospectos para estudiar en esta institución, podremos requerir los siguientes datos:

		•Nombre.
		•Estado Civil.
		•Fecha de nacimiento.
		•Correo electrónico.
		•Teléfono de casa, oficina y celular, en su caso.
		•Dirección.
		•Último nivel de estudios y nombre de la Institución.
		•Lugar de trabajo y sus datos, así como si el negocio es propio y fecha de antigüedad.
		•Programa escolar de interés.
		•Ciclo escolar de interés.
		•La forma en que se enteró de los programas escolares de la Institución.
		•Cualquier otro dato que sea necesario para la debida prestación de los servicios de la Institución.
		En caso de no contar con dicha información, la Institución se vería imposibilitada para inscribir y realizar cualquier trámite relacionado con el Titular de dichos datos.

		Hacemos de su conocimiento que para cumplir con las finalidades previstas en este Aviso de Privacidad y la Ley aplicable, podrán ser recabados y tratados datos personales sensibles, los cuales pueden relacionarse a su esfera más íntima, tales como aspectos de origen racial o étnico, estado de salud, creencias religiosas, filosóficas y morales, opiniones políticas, entre otras.

		Con base en lo anterior, nos comprometemos a que los mismos serán tratados bajo estrictas medidas de seguridad y garantizando su confidencialidad.

		Para las finalidades indicadas en el presente Aviso de Privacidad, la Institución puede recabar sus datos personales de la siguiente manera:

		Proporcionados directamente por el Titular de los datos.
		Obtenidas a través de otras fuentes permitidas por Ley.
		Por visitas al sitio de Internet de la Institución o el uso de sus servicios en línea.
		Cookies y Web Beacons

		Cabe mencionar, que al acceder a sitios de internet se podrán encontrar con “cookies”, que son archivos de texto que se descargan automáticamente y son almacenados en el disco duro del equipo de cómputo del usuario al navegar una página de internet específica, los cuales permiten grabar en el servidor de Internet algunos de sus datos.

		Igualmente, las páginas de Internet pueden contener “web beacons”, que son imágenes insertadas en la página o correo electrónico, que pueden ser utilizadas para monitorear el comportamiento de un visitante, así como para almacenar información sobre la dirección IP del usuario, duración del tiempo de interacción de dicha página y el tipo de navegador utilizado, entre otros.

		En virtud de lo anterior, le informamos que la Institución pudiera utilizar “cookies” y “web beacons” para un mejor desempeño del sitio.

		Estas “cookies” y otras tecnologías pueden ser deshabilitadas en las opciones de configuración del navegador que se esté usando.

		Esta declaración está sujeta a los términos y condiciones de todos los sitios web de la institución antes descritos, lo cual constituye un acuerdo legal entre el usuario y la institución. Si el Titular utiliza los servicios en cualquiera de los sitios de la institución, significa que ha leído, entendido y aceptado los términos antes expuestos. Si no está de acuerdo con ellos, el Titular no deberá proporcionar información personal, ni utilizar los servicios de los sitios de la institución.

		En el caso de empleo de cookies, el botón de “ayuda” que se encuentra en la barra de herramientas de la mayoría de los navegadores, le dirá cómo evitar aceptar nuevos cookies, cómo hacer que el navegador le notifique cuando recibe un nuevo cookie o cómo deshabilitar todos los cookies.

		La seguridad y la confidencialidad de los datos que los usuarios proporcionen estarán protegidos por un servidor seguro bajo el protocolo Secure Socket Layer (SSL), de tal forma que los datos enviados se transmitirán encriptados para asegurar su resguardo.

		Para verificar que se encuentra en un entorno protegido asegúrese de que aparezca una S en la barra de navegación (Ejemplo: httpS://.) Sin embargo, y a pesar de contar cada día con herramientas más seguras, la protección de los datos enviados a través de Internet no se puede garantizar al 100%; por lo que una vez recibidos, se hará todo lo posible por salvaguardar la información.

		Sobre Promociones, Ofertas y Servicios

		Nuestro programa publicitario de notificación de promociones, ofertas y servicios a través de correo electrónico, se realiza mediante mensajes promocionales de la Institución y, ocasionalmente, podrán incluirse ofertas de terceras partes que sean nuestros socios comerciales. Los correos electrónicos sólo serán enviados a los usuarios y a aquellos contactos registrados. Esta indicación podrá ser modificada en cualquier momento a través de los correos electrónicos enviados

		.
		En todo momento usted podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales y/o sensibles, en su caso, a fin de que dejemos de hacer uso de los mismos

		.
		Ejercicio de Derechos A.R.C.O.

		En caso de que desee ejercer sus derechos A.R.C.O. (acceder, rectificar, cancelar u oponerse al uso de sus datos personales) respecto a los datos que conforman la base de datos de la Institución, deberá cumplir con los siguientes requisitos:

		1. El Titular, o su representante legal debidamente acreditado, deberá Enviar una solicitud, por escrito al Departamento de Servicios Escolares del campus que corresponda. Dicha solicitud deberá contener: Nombre, domicilio, teléfono o correo electrónico del Titular y una descripción clara y precisa de los datos personales respecto de los que se busca ejercer alguno de los derechos mencionados.

		2. La Solicitud deberá acompañar los documentos que acrediten la Titularidad de los datos, y/o los documentos que lo acrediten como padre, tutor o representante legal del Titular; asimismo, deberá acompañar cualquier otro documento que facilite la localización de sus datos personales.

		4. En un plazo que no excederá de los 30 días hábiles, se dará respuesta al Titular de los datos en la oficina en donde ingresó la solicitud correspondiente.

		5. Para el caso de los datos personales recabados en las oficinas corporativas, se deberá dirigir el escrito al Titular del Área Administrativa que corresponda, el cual dará atención a la solicitud en los términos aquí descritos.

		Nos reservamos el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente Aviso de Privacidad, para la atención de novedades legislativas, jurisprudenciales, políticas internas, nuevos requerimientos solicitados por la Institución, entre otras.

		Estas modificaciones estarán disponibles al público a través de los siguientes medios: la página de internet de la Institución http://www.uvmmexico.mx/ el Departamento de servicios escolares y el Departamento de Mercadotecnia de cada campus.

		Igualmente le notificamos que este Aviso de Privacidad fue modificado el día 18 de febrero de 2014.
	</p>
</section>

<?php
include ('includes/footer.php');
?>

