<?php

include "includes/funciones.php";
$titulo = "Nosotros";
include ('includes/header3.php');

//obtener testos sider
$query = "SELECT * FROM empresa";

//#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);

while ($texto = $resultado -> fetch_assoc()) {
	$empresa1 = $texto['empresa'];
	$somos = $texto['somos'];
	$direccion = $texto['direccion'];
	$filosofia = $texto['filosofia'];
	$eslogan = $texto['eslogan'];
}
?>




<!-- BreadCrumb -->
<section id="ccr-left-section" class="col-md-12">
	<div class="current-page">
		<a href="index.php"><i class="fa fa-home"></i> <i class="fa fa-angle-double-right"></i></a> <?= $titulo ?>
	</div> <!-- / .current-page -->


				<section class="callout-content">

					<article class="col-md-6 col-sm-12">
						<h3><?= $empresa1 ?></h3>


						<p>
							<?= $somos ?>
						</p>

					</article>

						<div class="row">
							<div class="col-md-10 col-sm-12">
					    	</div>
                        </div>

                    <div class="tabs" style="margin-bottom: 300px">
                        <div class="tab">
                            <button class="tab-toggle">Historia</button>
                        </div>
                        <div class="content">
                            <h3 class="heading">Resumen</h3>
                            <p class="description">
                                La Asociación Mexicana de Voluntariado Internacional A.C. nace el 5 de diciembre del 2014 a iniciativa de ex-voluntarios mexicanos y profesionistas deseosos de participar en la transformación de país a través de la integración del Voluntariado Internacional en proyectos organizados en México por asociaciones civiles, ayuntamientos, comunidades y grupos organizados, así como con el envío de mexicanos a proyectos en el extranjero con la idea de que con esta experiencia puedan, al regreso al país, involucrarse de una forma más activa en el quehacer de la comunidad.
                                    <br>
                                Las primeras reuniones tuvieron lugar en la ciudad de Cuautla, Morelos, región central de México de donde son originarios varios de los fundadores.
                            </p>
                        </div>
                        <div class="tab">
                            <button class="tab-toggle">Visión</button>
                        </div>
                        <div class="content">
                            <h3 class="heading">Visión</h3>
                            <p class="description">
                                La asociación tiene como visión la transformación de la sociedad mexicana, en particular de la juventud, dándoles oportunidades de viajar y vivir experiencias únicas como voluntarios tanto en México como en el extranjero participando en proyectos de interés general y revirtiendo esa experiencia en sus comunidades, escuelas, familias, colonias, organizaciones y en sus empleos.
                            </p>
                        </div>
                        <div class="tab">
                            <button class="tab-toggle">Objetivos</button>
                        </div>
                        <div class="content">
                            <h3 class="heading">
                                Objetivos
                            </h3>
                            <p class="description">
                                AMVIAC tiene por objetivos :

                            </p>
                            <ol class="rounded-list">
                                <li>
                                    <a href="javascript:void(0)">
                                    Informar a jóvenes y adultos sobre el Voluntariado Internacional y las posibilidades de participación.
                                        </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                    -	Divulgar los valores y logros del voluntariado internacional a través de espacios de debate y comunicación, especialmente los virtuales basados en las herramientas que ofrecen las nuevas tecnologías de la información y la comunicación.
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                    -	Dotar a las personas de los conocimientos básicos sobre el voluntariado internacional, la organización y la realidad sobre la que se actúa, mediante la capacitación y promoción.
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                    -	Realizar formaciones específicas de las personas voluntarias para el desarrollo de su actividad, preparación, seguimiento y evaluación.
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                    -	Coordinar el envío de voluntarios mexicanos a proyectos en México y en el extranjero así como la acogida de voluntarios internacionales en proyectos en nuestro país.
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                    -	Estudiar la realidad jurídica existente y promover en consecuencia los marcos jurídicos que permitan potenciar el voluntariado internacional.
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                    -	Fomentar y gestionar el voluntariado internacional vinculándolo a los proyectos de acción social, medioambiental, patrimonial y cultural.
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                    -	Fortalecer el tejido social en los diversos espacios geográficos de intervención, fomentando el voluntariado internacional en los distintos recursos de la comunidad (centros educativos y culturales, servicios sociales, centros socio-sanitarios, asociaciones civiles…)
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                    -	Facilitar un punto de encuentro e información para los/as jóvenes promocionando el voluntariado internacional como una opción de crecimiento y desarrollo personal.
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                    -	Participar en el proceso e implementación de los Objetivos de Desarrollo Sustentable-Post2015 promovidos por la ONU.
                                    </a>
                                </li>
                            </ol>

                        </div>
                    </div>


				</section>

	</section>




<?php
if ($titulo == "Inicio") {
	include "includes/footer3.php";
} else {
	include "includes/footer2.php";
}
?>
<script>
	// ----------------- Variables

	wrapper = $(".tabs");
	tabs = wrapper.find(".tab");
	tabToggle = wrapper.find(".tab-toggle");

	// ----------------- Functions

	function openTab() {
		var content = $(this).parent().next(".content"), activeItems = wrapper.find(".active");

		if (!$(this).hasClass('active')) {
			$(this).add(content).add(activeItems).toggleClass('active');
			wrapper.css('min-height', content.outerHeight());
		}
	};

	// ----------------- Interactions

	tabToggle.on('click', openTab);

	// ----------------- Constructor functions

	$(window).load(function() {
		tabToggle.first().trigger('click');
	}); 
</script>