</div><!-- /.container -->
</section><!-- / #ccr-main-section -->

<aside id="ccr-footer-sidebar">
	<div class="container">

		<ul>
			<li class="col-md-3">
				<h5>Acerca de AMVIAC</h5>
				<div class="about-us">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro, quod, nostrum, corrupti, maxime quis doloribus debitis id consectetur laudantium iure aperiam soluta consequuntur modi accusamus molestias. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro, quod, nostrum, corrupti, maxime quis doloribus debitis id consectetur laudantium iure aperiam soluta consequuntur modi accusamus molestias Ab veniam atque eius...
				</div>
				<div class="site-logo">
					<a href="index.php"><img src="admin/img/logo.png" alt="Side Logo" />
				</div>
				<!-- / .navbar-header -->

			</li>
			<li class="col-md-3">
				<h5>Entradas Populares</h5>
				<ul>
					<li>
						<img src="img/thumbnail-small-1.jpg" alt="Avatar">
						<a href="#">Lorem Ipsum is a samply dummy text of Popular Post</a>
					</li>
					<li>
						<img src="img/sports-thumb-4.jpg" alt="Avatar">
						<a href="#">Lorem Ipsum is a samply dummy text of Popular Post</a>
					</li>
					<li>
						<img src="img/thumbnail-small-3.jpg" alt="Avatar">
						<a href="#">Lorem Ipsum is a samply dummy text of Popular Post</a>
					</li>
					<li>
						<img src="img/thumbnail-small-5.jpg" alt="Avatar">
						<a href="#">Lorem Ipsum is a samply dummy text of Popular Post</a>
					</li>
				</ul>

			</li>
			<li class="col-md-3">
				<h5>Entradas recientes</h5>
				<ul>
					<li>
						<img src="img/thumbnail-small-8.jpg" alt="Avatar">
						<a href="#">Lorem Ipsum is a samply dummy text of Recent Post</a>
					</li>
					<li>
						<img src="img/sports-thumb-6.jpg" alt="Avatar">
						<a href="#">Lorem Ipsum is a samply dummy text of Recent Post</a>
					</li>
					<li>
						<img src="img/thumbnail-small-7.jpg" alt="Avatar">
						<a href="#">Lorem Ipsum is a samply dummy text of Recent Post</a>
					</li>
					<li>
						<img src="img/thumbnail-small-6.jpg" alt="Avatar">
						<a href="#">Lorem Ipsum is a samply dummy text of Recent Post</a>
					</li>
				</ul>

			</li>
			<li class="col-md-3">
				<h5>Etiquetas</h5>
				<div class="tagcloud">
					<a href="#">Voluntariado</a>
					<a href="#">Actividades</a>
					<a href="#">AMVIAC</a>
					<a href="#">Cuautla</a>
					<a href="#">Eventos</a>
					<a href="#">Galería</a>

				</div>

			</li>
		</ul>
	</div>

</aside>
<!-- /#ccr-footer-sidebar -->

<footer id="ccr-footer">
	<div class="container">
		<div class="copyright">
			&copy; <?= date('Y'); ?>, Derechos Reservados <a href="http://amviac.hostinazo.com">AMVIAC</a>.
		</div>
		<!-- /.copyright -->

		<div class="footer-social-icons">
			<ul>
				<li>
					<a href="#"  class="google-plus"><i class="fa fa-google-plus"></i></a>
				</li>
				<li >
					<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
				</li>
				<li >
					<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
				</li>
				<li >
					<a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
				</li>
			</ul>

		</div><!--  /.cocial-icons -->

	</div>
	<!-- /.container -->
</footer>
<!-- /#ccr-footer -->

<!-- end remprod -->

<script type="text/javascript" src="assets/js/_jq.js"></script>
<script type="text/javascript" src="assets/js/_jquery.placeholder.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/activeaxon_menu.js" type="text/javascript"></script>
<script src="assets/js/animationEnigne.js" type="text/javascript"></script>
<script src="assets/js/arwa.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/ie-fixes.js" type="text/javascript"></script>
<script src="assets/js/jquery.base64.js" type="text/javascript"></script>
<script src="assets/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>
<script src="assets/js/jquery.cycle.js" type="text/javascript"></script>
<script src="assets/js/jquery.cycle2.carousel.js" type="text/javascript"></script>
<script src="assets/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="assets/js/jquery.easytabs.js" type="text/javascript"></script>
<script src="assets/js/jquery.eislideshow.js" type="text/javascript"></script>
<script src="assets/js/jquery.isotope.js" type="text/javascript"></script>
<script src="assets/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="assets/js/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.themepunch.revolution.js" type="text/javascript"></script>
<script src="assets/js/jquery.tipsy.js" type="text/javascript"></script>
<script src="assets/js/jquery.validate.js" type="text/javascript"></script>
<script src="assets/js/jQuery.XDomainRequest.js" type="text/javascript"></script>
<!--<script src="assets/js/retina.js" type="text/javascript"></script>-->
<script src="assets/js/timeago.js" type="text/javascript"></script>
<!--<script src="assets/js/tweetable.jquery.js" type="text/javascript"></script>-->


</body>

</html>
