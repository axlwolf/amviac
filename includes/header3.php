<?php
include "admin/Conexion.php";
//obtener menus
$query = "SELECT * FROM menu WHERE estado='s'";

#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<!--[if IE]>
	<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<title><?= $titulo; ?> || AMVIAC</title>

	<!--[if lt IE 9]>
	<script type="text/javascript" src="js/ie-fixes.js"></script>
	<link rel="stylesheet" href="assets/css/ie-fixes.css">
	<script type="text/javascript" src="js/_respond.min.js"></script>
	<![endif]-->

	<meta name="description" content="<?= $titulo; ?>">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- icono en el navegador -->
    <link rel="shortcut icon" href="img/logo-sitio.ico" />

	<!--- This should placed first off all other scripts -->


<!--	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">-->
<!--	<link rel="stylesheet" href="css/bootflat.css"/>-->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/owl.carousel.css"/>
    <link rel="stylesheet" type="text/css" href="css/stilosAbd.css" />



	<!--	<link rel="stylesheet" href="assets/css/normalize.css">-->



<!--	<link href='assets/css/style.css' rel='stylesheet' type='text/css'>-->
<!--	<link href='assets/css/responsive.css' rel='stylesheet' type='text/css'>-->


<!--	<link href="assets/css/skins/one-blue.css" rel='stylesheet' type='text/css' id="skin-file">-->
<!--	<link rel="stylesheet" href="assets/css/font-awesome-ie7.css">-->
<!--	<link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css">-->
<!--	<link rel="stylesheet" href="assets/css/font-awesome.css">-->
<!--	<link rel="stylesheet" href="assets/css/font-awesome.min.css">-->
	<link rel="stylesheet" href="assets/css/revolution_settings.css">
	<link rel="stylesheet" href="assets/css/eislider.css">
	<link rel="stylesheet" href="assets/css/tipsy.css">
	<link rel="stylesheet" href="assets/css/prettyPhoto.css">
	<link rel="stylesheet" href="assets/css/isotop_animation.css">

<!---->
<!--	<!-- remprod -->
<!--	<style type="text/css" id="skin-chooser-css">-->
<!--	</style>-->
<!--	<!-- end remprod -->


	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Cuprum:400,400italic,700italic,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,200,300' rel='stylesheet' type='text/css'>

    <style>

        button,
        .button {
            position: relative;
            display: inline-block;
            color: white;
            padding: 0.75rem 2rem;
            margin: 0 auto;
            background-color: #374d72;
            border: none;
            width: 100%;
            font-weight: bold;
            font-size: 1.2rem;
            text-align: center;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
            
        }
        button:hover,
        .button:hover {
            background-color: #374d72;
        }
        button:hover:before, button:hover:after,
        .button:hover:before,
        .button:hover:after {
            color: #4cc4cf;
        }
        button:after, button:before,
        .button:after,
        .button:before {
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
        }

        /* ---------------------- Vertical Tabs */
        .tabs {
            margin: 25px;
            position: relative;
            min-height: 0;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            transition: all 0.5s;
            /* ---------------------- Tab */
            /* ---------------------- Content */
        }
        .tabs .tab {
            margin-bottom: 1px;
            /* ---------------------- Tab Toggle */
        }
        @media screen and (min-width: 55rem) {
            .tabs .tab {
                float: left;
                clear: left;
                width: 30%;
            }
        }
        .tabs .tab .tab-toggle {
            padding: 1rem 4rem 1rem 2rem;
            position: relative;
            outline: none;
            width: 100%;
        }
        @media screen and (min-width: 55rem) {
            .tabs .tab .tab-toggle {
                text-align: left;
            }
        }
        .tabs .tab .tab-toggle:after {
            content: "\25BC";
            position: absolute;
            display: block;
            right: 2rem;
            top: 50%;
            -webkit-transform: rotate(0deg) translateY(-50%);
            -moz-transform: rotate(0deg) translateY(-50%);
            -ms-transform: rotate(0deg) translateY(-50%);
            -o-transform: rotate(0deg) translateY(-50%);
            transform: rotate(0deg) translateY(-50%);
        }
        @media screen and (min-width: 55rem) {
            .tabs .tab .tab-toggle:after {
                -webkit-transform: rotate(-90deg) translateX(50%);
                -moz-transform: rotate(-90deg) translateX(50%);
                -ms-transform: rotate(-90deg) translateX(50%);
                -o-transform: rotate(-90deg) translateX(50%);
                transform: rotate(-90deg) translateX(50%);
            }
        }
        .tabs .tab .tab-toggle.active {
            color: #8099c2;
            background-color: white;
            cursor: default;
        }
        .tabs .tab .tab-toggle.active:after {
            color: #8099c2;
            -webkit-transform: rotate(180deg) translateY(50%);
            -moz-transform: rotate(180deg) translateY(50%);
            -ms-transform: rotate(180deg) translateY(50%);
            -o-transform: rotate(180deg) translateY(50%);
            transform: rotate(180deg) translateY(50%);
        }
        @media screen and (min-width: 55rem) {
            .tabs .tab .tab-toggle.active:after {
                -webkit-transform: rotate(-90deg) translateX(50%) translateY(0);
                -moz-transform: rotate(-90deg) translateX(50%) translateY(0);
                -ms-transform: rotate(-90deg) translateX(50%) translateY(0);
                -o-transform: rotate(-90deg) translateX(50%) translateY(0);
                transform: rotate(-90deg) translateX(50%) translateY(0);
                right: 1rem;
            }
        }
        .tabs .content {
            max-height: 0;
            overflow: hidden;
            padding: 0 2rem;
            background-color: #efefef;
            -webkit-transition: all 1s;
            -moz-transition: all 1s;
            transition: all 1s;
        }
        @media screen and (min-width: 55rem) {
            .tabs .content {
                max-height: none;
                position: absolute;
                right: 0;
                top: 0;
                width: 70%;
                opacity: 0;
                padding: 0rem 2rem 2rem 2rem;
                -webkit-transform: translateX(100%);
                -moz-transform: translateX(100%);
                -ms-transform: translateX(100%);
                -o-transform: translateX(100%);
                transform: translateX(100%);
            }
        }
        .tabs .content.active {
            max-height: 1000px;
        }
        @media screen and (min-width: 55rem) {
            .tabs .content.active {
                max-height: none;
                opacity: 1;
                -webkit-transform: none;
                -moz-transform: none;
                -ms-transform: none;
                -o-transform: none;
                transform: none;
            }
        }
        .tabs .content > .heading {
            font-size: 1.5rem;
            margin-bottom: 1rem;
        }

        .tabs .content > p.description, ul.description {
            text-align: justify;
        }

        ul.description{
            font-size: 12px;
        }




        /* -------------------------------------- */

        .rounded-list a {
            position: relative;
            display: block;
            padding: .4em .4em .54em 3em;
            *padding: .4em;
            margin: .5em 0;
            background: #ddd;
            color: #444;
            text-decoration: none;
            -moz-border-radius: .3em;
            -webkit-border-radius: .3em;
            border-radius: .3em;
            -webkit-transition: all .3s ease-out;
            -moz-transition: all .3s ease-out;
            -ms-transition: all .3s ease-out;
            -o-transition: all .3s ease-out;
            transition: all .3s ease-out;
        }

        .rounded-list a:hover {
            background: #eee;
        }

        .rounded-list a:hover:before {
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }

        .rounded-list a:before {
            content: counter(li);
            counter-increment: li;
            position: absolute;
            left: -1.3em;
            top: 50%;
            margin-top: -1.3em;
            background: #87ceeb;
            height: 2em;
            width: 2em;
            line-height: 2em;
            border: .3em solid #fff;
            text-align: center;
            font-weight: bold;
            -moz-border-radius: 2em;
            -webkit-border-radius: 2em;
            border-radius: 2em;
            -webkit-transition: all .3s ease-out;
            -moz-transition: all .3s ease-out;
            -ms-transition: all .3s ease-out;
            -o-transition: all .3s ease-out;
            transition: all .3s ease-out;
        }

        /* -------------------------------------- */

        .rectangle-list a {
            position: relative;
            display: block;
            padding: .4em .4em .4em .8em;
            *padding: .4em;
            margin: .5em 0 .5em 2.5em;
            background: #ddd;
            color: #444;
            text-decoration: none;
            -webkit-transition: all .3s ease-out;
            -moz-transition: all .3s ease-out;
            -ms-transition: all .3s ease-out;
            -o-transition: all .3s ease-out;
            transition: all .3s ease-out;
        }

        .rectangle-list a:hover {
            background: #eee;
        }

        .rectangle-list a:before {
            content: counter(li);
            counter-increment: li;
            position: absolute;
            left: -2.5em;
            top: 50%;
            margin-top: -1em;
            background: #fa8072;
            height: 2em;
            width: 2em;
            line-height: 2em;
            text-align: center;
            font-weight: bold;
        }

        .rectangle-list a:after {
            position: absolute;
            content: '';
            border: .5em solid transparent;
            left: -1em;
            top: 50%;
            margin-top: -.5em;
            -webkit-transition: all .3s ease-out;
            -moz-transition: all .3s ease-out;
            -ms-transition: all .3s ease-out;
            -o-transition: all .3s ease-out;
            transition: all .3s ease-out;
        }

        .rectangle-list a:hover:after {
            left: -.5em;
            border-left-color: #fa8072;
        }

        /* -------------------------------------- */

        .circle-list li {
            padding: 3.5em;
            border-bottom: 1px dashed #ccc;
        }

        .circle-list h2 {
            position: relative;
            margin: 0;
        }

        .circle-list p {
            margin: 0;
        }

        .circle-list h2:before {
            content: counter(li);
            counter-increment: li;
            position: absolute;
            z-index: -1;
            left: -1.3em;
            top: -.8em;
            background: #f5f5f5;
            height: 1.5em;
            width: 1.5em;
            border: .1em solid rgba(0,0,0,.05);
            text-align: center;
            font: italic bold 1em/1.5em Georgia, Serif;
            color: #ccc;
            -moz-border-radius: 1.5em;
            -webkit-border-radius: 1.5em;
            border-radius: 1.5em;
            -webkit-transition: all .2s ease-out;
            -moz-transition: all .2s ease-out;
            -ms-transition: all .2s ease-out;
            -o-transition: all .2s ease-out;
            transition: all .2s ease-out;
        }

        .circle-list li:hover h2:before {
            background-color: #ffd797;
            border-color: rgba(0,0,0,.08);
            border-width: .2em;
            color: #444;
            -webkit-transform: scale(1.5);
            -moz-transform: scale(1.5);
            -ms-transform: scale(1.5);
            -o-transform: scale(1.5);
            transform: scale(1.5);
        }
        ol {
            counter-reset: li;
            list-style: none;
            padding: 0;
            margin-bottom: 4em;
            text-shadow: 0 1px 0 rgba(255,255,255,.5);
        }
        ul {
            counter-reset: li;
            list-style: none;
            padding: 0;
            margin-bottom: 4em;
            text-shadow: 0 1px 0 rgba(255,255,255,.5);
        }
        p{
            margin-bottom: 15px;
            text-align: justify;
        }

        /*#myModal3*/
        /*{*/
            /*width: 950px;*/

        /*}*/


            </style>
</head>
<div id="fb-root"></div>
<script>
	( function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk')); 
</script>


<body>
<header id="ccr-header">
<!--	<section id="ccr-nav-top" class="fullwidth">-->
<!--		<div class="">-->
<!--			<div class="container">-->
<!--				<nav class="top-menu">-->
<!--					<ul class="left-top-menu">-->
<!--						<li><a href="index.php">Inicio</a></li>-->
<!--						<li><a href="#">Mapa del Sitio</a></li>-->
<!--						<li><a href="contacto.php">Contacto</a></li>-->
<!--					</ul><!-- /.left-top-menu -->
<!---->
<!--					<ul class="right-top-menu pull-right">-->
<!--						<li><a href="#">Login</a></li>-->
<!--						<li><a href="#">Registro</a></li>-->
<!--						<li><a href="#">Subscribirse</a></li>-->
<!--						<li>-->
<!--							<form class="form-inline" role="form" action="#">-->
<!--								<input type="search" placeholder="Buscar..." required>-->
<!--								<button type="submit"><i class="fa fa-search"></i></button>-->
<!--							</form>-->
<!--						</li>-->
<!--					</ul> <!--  /.right-top-menu -->
<!--				</nav> <!-- /.top-menu -->
<!--			</div>-->
<!--		</div>-->
<!--	</section> <!--  /#ccr-nav-top  -->



	<section id="ccr-site-title">
		<div class="container">

			<div class="site-logo">
				<a href="index.php" class="navbar-brand">
					<img src="img/logo-sitio.png" alt="Side Logo" />

				</a>
			</div> <!-- / .navbar-header -->


		</div>	<!-- /.container -->
	</section> <!-- / #ccr-site-title -->



	<section id="ccr-nav-main">
		<nav class="main-menu">
			<div class="container">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".ccr-nav-main">
						<i class="fa fa-bars"></i>
					</button> <!-- /.navbar-toggle -->
				</div> <!-- / .navbar-header -->

				<div class="collapse navbar-collapse ccr-nav-main">
					<ul class="nav navbar-nav">
						<?php


						while($menu = $resultado->fetch_assoc()):
							?>
							<li><a class="nav-link" href="<?= $menu['url'] ?>"><?= $menu['nombre'] ?></a></li>

						<?php

						endwhile;
						?>
						<?php

							echo '<li><a class="nav-link" href="secciones.php?codigo=11"</a>Noticias</li>';

						?>
					</ul> <!-- /  .nav -->
				</div><!-- /  .collapse .navbar-collapse  -->

				

			</div>	<!-- /.container -->
		</nav> <!-- /.main-menu -->
	</section> <!-- / #ccr-nav-main -->



</header> <!-- /#ccr-header -->

<section id="ccr-main-section">
	<div class="container">

