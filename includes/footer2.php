</div><!-- /.container -->
</section><!-- / #ccr-main-section -->


<footer id="ccr-footer">
    <div class="container">
        <div class="copyright">
            &copy; <?= date('Y'); ?>, Derechos Reservados <a href="http://amviac.hostinazo.com">AMVIAC</a>.
        </div>
        <!-- /.copyright -->

        <div class="footer-social-icons">
            <ul>
                
                <li><a href="#" class="youtube"><i class="fa fa-youtube"></i></a>
                <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                
            </ul>

        </div><!--  /.cocial-icons -->

    </div>
    <!-- /.container -->
</footer>
<!-- /#ccr-footer -->

<!-- end remprod -->

<script type="text/javascript" src="assets/js/_jq.js"></script>
<script type="text/javascript" src="assets/js/_jquery.placeholder.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/activeaxon_menu.js" type="text/javascript"></script>
<script src="assets/js/animationEnigne.js" type="text/javascript"></script>
<script src="assets/js/arwa.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/ie-fixes.js" type="text/javascript"></script>
<script src="assets/js/jquery.base64.js" type="text/javascript"></script>
<script src="assets/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>
<script src="assets/js/jquery.cycle.js" type="text/javascript"></script>
<script src="assets/js/jquery.cycle2.carousel.js" type="text/javascript"></script>
<script src="assets/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="assets/js/jquery.easytabs.js" type="text/javascript"></script>
<script src="assets/js/jquery.eislideshow.js" type="text/javascript"></script>
<script src="assets/js/jquery.isotope.js" type="text/javascript"></script>
<script src="assets/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="assets/js/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.themepunch.revolution.js" type="text/javascript"></script>
<script src="assets/js/jquery.tipsy.js" type="text/javascript"></script>
<script src="assets/js/jquery.validate.js" type="text/javascript"></script>
<script src="assets/js/jQuery.XDomainRequest.js" type="text/javascript"></script>
<!--<script src="assets/js/retina.js" type="text/javascript"></script>-->
<script src="assets/js/timeago.js" type="text/javascript"></script>
<!--<script src="assets/js/tweetable.jquery.js" type="text/javascript"></script>-->
<script type="text/javascript" src="js/buscador.js"></script>

</body>

</html>
