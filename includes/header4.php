<!DOCTYPE html>
<html lang="es" class="no-js">
	<head>
		<meta charset="utf-8">
		<title>Blanco</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<!--		estilos-->
		<link rel="stylesheet" href="css/normalize.css"/>
		<link rel="stylesheet" href="css/bootstrap.min.css"/>
		<link rel="stylesheet" href="css/bootflat.css"/>
		<link rel="stylesheet" href="css/estilos.css"/>
		<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<![endif]-->
		<script src="js/responsive-nav.js"></script>
	</head>
	<body>
		<div id="fb-root"></div>
		<script>
			( function(d, s, id) {
					var js, fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id))
						return;
					js = d.createElement(s);
					js.id = id;
					js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.0";
					fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
		</script>

		<h1 class="header"><a href="./" title="Ir al inicio"><img src="img/header.png" alt="Logo Liberum"/></a></h1>

		<div id="header">
			<div class="wrapper">
				<!-- START Social Links -->

				<div class="social-links">
					<a href="#"	id="btn-comunidad"	rel="nofollow" target="_blank">Comunidad UVM</a>
					<a href="#"	id="btn-facebook"	rel="nofollow" target="_blank">Facebook</a>
					<a href="#"	id="btn-twitter"	rel="nofollow" target="_blank">Twitter</a>
					<a href="#"	id="btn-youtube"	rel="nofollow" target="_blank">YouTube</a>
					<a href="#"	id="btn-rss"		rel="nofollow" target="_blank">RSS</a>
				</div>
				<!-- END Social Links -->

			</div>
		</div>

		<header>
			<!--  <a href="index.php" class="logo" data-scroll><h5><?php echo $empresa; ?></h5></a>-->
			<nav class="nav-collapse">
				<ul>
					<?php
					$can = mysql_query("SELECT * FROM tipos WHERE estado='s'");
					while ($dato = mysql_fetch_array($can)) {
						echo '<li><a class="nav-link" href="secciones.php?codigo=' . $dato['id'] . '">' . $dato['nombre'] . '</a></li>';
					}
					?>
				</ul>
			</nav>

		</header>

		<div class="container">
