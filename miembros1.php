<?php
$titulo = "Miembros";
include ('includes/header3.php');

//obtener testos sider
$query = "SELECT * FROM empresa";

//#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);

while ($texto = $resultado -> fetch_assoc()) {
	$empresa1 = $texto['empresa'];
	$somos = $texto['somos'];
	$direccion = $texto['direccion'];
	$filosofia = $texto['filosofia'];
	$eslogan = $texto['eslogan'];
}
?>
<section id="ccr-left-section" class="col-md-12">

	<div class="current-page">
		<a href="index.php"><i class="fa fa-home"></i> <i class="fa fa-angle-double-right"></i></a> <?= $titulo ?>
	</div>
	<!-- / .current-page -->

	<section style="margin-bottom: 600px">
		<div class="tabbable tabs-vertical tabs-left">
			<ul id="myTab" class="nav nav-tabs">
				<li class="active">
					<a href="#asamblea" data-toggle="tab">Asamblea General</a>
				</li>
				<li>
					<a href="#consejo" data-toggle="tab">Consejo Nacional</a>
				</li>
				<li>
					<a href="#junta" data-toggle="tab">Junta Directiva</a>
				</li>
				<li>
					<a href="#secretariado" data-toggle="tab">Secretariado</a>
				</li>
				<li>
					<a href="#miembros" data-toggle="tab">Miembros</a>
				</li>
				<li>
					<a href="#como-ser" data-toggle="tab">¿Cómo ser miembro?</a>
				</li>
				<li>
					<a href="#fondo" data-toggle="tab">Fondo Internacional de Solidaridad</a>
				</li>
			</ul>
			<div id="myTabContent" class="tab-content">

				<div class="tab-pane fade in active" id="asamblea">
					<h1 class="page-header">Asamblea General</h1>

					<p>
						La Asamblea General es el órgano supremo de gobierno de la Asociación, integrado por los/las asociados/as, que adopta sus acuerdos por el principio mayoritario o de democracia interna. La Asamblea General se reúne una vez al año dentro de los cuatro meses siguientes al cierre del ejercicio.
					</p>
					<p>
						Es responsabilidad de la Asamblea General determinar las orientaciones políticas y prioridades de la asociación para el año en curso así como determinar las cuotas de membresía y aprobar las actividades propuestas por la Junta Directiva.
					</p>

				</div>
				<div class="tab-pane fade" id="consejo">

					<h1 class="page-header">Consejo Nacional</h1>
					<p>
						La Asociación se rige por un Consejo Nacional de entre 12 miembros como mínimo y 18 miembros máximo, elegidos por 2 años en votación secreta por la Asamblea General de entre los miembros activos.
					</p>
					<p>
						La renovación del Consejo Nacional se lleva a cabo por terceras partes cada año, de acuerdo a la antigüedad. Los miembros del Consejo Nacional son responsables de tomar todas las medidas para garantizar la ejecución de los proyectos organizados por la asociación.
					</p>
					<p>
						El Consejo Nacional se reúne por lo menos trimestralmente y siempre que lo convoque su Presidente o a petición de la mayoría de sus miembros.
					</p>
					<hr>
					<h2 class="sub-header"> En el 2015 los miembros del Consejo Nacional son: </h2>
					<ol class="rounded-list">
						<li>
							<a href="javascript:void(0)"> María Isabel Cervantes Tapia </a>
						</li>
						<li>
							<a href="javascript:void(0"> Violetta Farías Aguirre </a>
						</li>
						<li>
							<a href="javascript:void(0"> Guadalupe Rangel Romero </a>
						</li>
						<li>
							<a href="javascript:void(0"> Alberto Velasco </a>
						</li>

						<li>
							<a href="javascript:void(0"> Claudia Patricia Vega Bermúdez </a>
						</li>
						<li>
							<a href="javascript:void(0"> David Castro </a>
						</li>
						<li>
							<a href="javascript:void(0"> Lorenzo Elizalde </a>
						</li>
						<li>
							<a href="javascript:void(0"> Jaime Genis </a>
						</li>
						<li>
							<a href="javascript:void(0"> Francisco Axel Lanuza Rojas </a>
						<li>
							<a href="javascript:void(0"> Lennin Vick Pacheco Sánchez </a>
						</li>
						<li>
							<a href="javascript:void(0"> Daniel Salinas Córdova </a>

						</li>
						<li>
							<a href="javascript:void(0"> Maurice Edgar Aguilar Osorio </a>

						</li>
						<li>
							<a href="javascript:void(0"> Roberto Arcos Ávila </a>

						</li>
						<li>
							<a href="javascript:void(0"> Manuel Prado </a>

						</li>
					</ol>

				</div>
				<div class="tab-pane fade" id="junta">
					<h1 class="page-header">Junta Directiva</h1>
					<p>
						La asociación es gestionada y representada por una Junta Directiva. Todos los cargos que componen la Junta Directiva son voluntarios. Éstos son designados y revocados por la Asamblea General y su mandato tiene una duración de 2 años y puede ser reelegible. La Junta Directiva se reúne cuantas veces lo determine su Presidente y a iniciativa o petición de la mitad de sus miembros.
					</p>
					<hr>
					<p>
						La Junta Directiva dirige las actividades sociales y lleva la gestión económica y administrativa de la Asociación, ejecuta los acuerdos de la Asamblea General, lleva una contabilidad conforme a las normas específicas que permite obtener la imagen fiel del patrimonio, del resultado y de la situación financiera de la asociación, formula y somete a la aprobación de la Asamblea General los Balances y las Cuentas anuales, nombra delegados/as para alguna determinada actividad de la Asociación y asume cualquier otra facultad que no sea de la exclusiva competencia de la Asamblea General de socios/as.
					</p>

					<h2> La Junta Directiva en el 2015 está compuesta por: </h2>

					<ol class="rounded-list">
						<li>
							<a href="javascript:void(0)"> María Isabel Cervantes Tapia </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Violetta Farías Aguirre </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Guadalupe Rangel Romero </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Alberto Velasco </a>
						</li>
						<li>
							<a href="javascript:void(0)"> David Castro </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Lorenzo Elizalde </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Jaime Genis </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Lennin Vick Pacheco Sánchez </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Daniel Salinas Córdova </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Maurice Edgar Aguilar Osorio </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Roberto Arcos Ávila </a>
						</li>

					</ol>

				</div>

				<div class="tab-pane fade" id="secretariado">
					<h1 class="page-header">Secretariado</h1>
					<p>
						El Secretariado está compuesto por el Director y su equipo quienes son responsables por la gestión y administración cotidiana de la asociación, mantienen los vínculos con los miembros y con las organizaciones socias nacionales e internacionales, gestionan a los voluntarios, dan seguimiento a proyectos y mantienen informada a la asociación sobre el desarrollo de la institución. Es responsabilidad del Director mantener un buen funcionamiento del secretariado y reporta directamente a la Mesa Directiva y al Consejo Nacional sobre la implementación de los acuerdos  y decisiones de la Asamblea general.

					</p>
					<hr>

				</div>
				<div class="tab-pane fade" id="fondo">
					<h1 class="page-header">Fondo Internacional de Solidaridad</h1>
					<p>
						Es un fondo creado para apoyar la participación de jóvenes de bajos recursos y pocas oportunidades en algún proyecto como voluntario ya sea en México o en el extranjero. Este fondo se genera durante un año y se utiliza el año siguiente.
					</p>
					<hr>

				</div>
				<div class="tab-pane fade" id="miembros">
					<h1 class="page-header">Miembros</h1><!-- Button trigger modal -->
					<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
						Miembros adherentes, activos y benefactores en el 2015
					</button>
					<br/>
					<hr/>

					<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<h4 class="modal-title" id="myModalLabel">Miembros adherentes, activos y benefactores en el 2015</h4>
								</div>
								<div class="modal-body">
									<table class="table table-bordered table-responsive">
										<tr>
											<td> Alberto Velasco </td>
											<td> Lennin Vick Pacheco Sánchez </td>
										</tr>

										<tr>
											<td> Allan Díaz </td>
											<td> Leo Rosas </td>
										</tr>
										<tr>
											<td> Amanda Tapia </td>
											<td> Leslie Feregrino Suárez </td>
										</tr>
										<tr>
											<td> Ana Maria Osorio Quintana </td>
											<td> Lita Ivette Chávez Sánchez </td>
										</tr>
										<tr>
											<td> Ausencio Miranda </td>
											<td> Lorenzo Lizalde </td>
										</tr>
										<tr>
											<td> Axel Lanuza Rojas </td>
											<td> Lucero Nicolas </td>
										</tr>
										<tr>
											<td> Blanca Gutiérrez Tapia </td>
											<td> Luis Abraham Aguilar Osorio </td>
										</tr>

										<tr>
											<td> Carlos Orozco </td>
											<td> Luis Edgardo Aguilar Cuéllar </td>
										</tr>
										<tr>
											<td> Carmen Duarte </td>
											<td> Magdalena Alemán </td>
										</tr>
										<tr>
											<td> Claudia Vega Bermúdez </td>
											<td> Manuel Prado </td>
										</tr>

										<tr>
											<td> Dafne Aguilar Osorio </td>
											<td> Maria Esther Saldaña </td>
										</tr>
										<tr>
											<td> Daniel Salinas </td>
											<td> María Helena Osorio Revilla </td>
										</tr>
										<tr>
											<td> Daniel Elizalde </td>
											<td> María Isabel Cervantes </td>
										</tr>
										<tr>
											<td> David Castro </td>
											<td> Mariel Denise </td>
										</tr>
										<tr>
											<td> Eduardo Arévalo </td>
											<td> Maurice Edgar Aguilar Osorio </td>
										</tr>
										<tr>
											<td> Emilio Guerra </td>
											<td> Maximino Sanabria </td>
										</tr>
										<tr>
											<td> Francisco Cisneros </td>
											<td> Nancy Patricia Suárez Osorio </td>
										</tr>
										<tr>
											<td> Gabriela De la Peña </td>
											<td> Nicolás Rodrigo Aguilar Osorio </td>
										</tr>
										<tr>
											<td> Gerardo Lozano </td>
											<td> Nora Eugenia Martínez Guzmán </td>
										</tr>
										<tr>
											<td> Guadalupe Rangel </td>
											<td> Paola Rosas Domínguez </td>

										</tr>
										<tr>
											<td> Guadalupe Santos </td>
											<td> Paz Matilde Osorio Revilla </td>

										</tr>
										<tr>
											<td> Homero Aguilar </td>
											<td> Roberto Arcos Ávila </td>
										</tr>
										<tr>
											<td> Jaime Genis </td>
											<td> Roberto Díaz Villanueva </td>
										</tr>
										<tr>
											<td> Laura Alejandri </td>
											<td> Rosa Morales </td>
										</tr>
										<tr>
											<td> Santiago Osorio Revilla </td>
											<td> Vanesa Maciel </td>
										</tr>
										<tr>
											<td> Senorina Osorio Revilla </td>
											<td> Violetta Farías </td>
										</tr>

									</table>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Cerrar
									</button>
								</div>
							</div>
						</div>
					</div>
					<p>
						Son miembros de la asociación todas las personas físicas y morales que, libre y voluntariamente, tienen interés en el desarrollo de los fines de la asociación con arreglo a los siguientes principios:
					</p>
					<ul class="rectangle-list">
						<li>
							<a href="javascript:void(0)"> a) Las personas físicas con capacidad de obrar y que no están sujetas a ninguna condición legal para el ejercicio del derecho. </a>
						</li>
						<li>
							<a href="javascript:void(0)"> b) Las instituciones, asociaciones o cualquier tipo de personas morales que desarrollen actividades de voluntariado internacional en el país, debiendo designar a un representante, persona física. La condición de asociado es intransmisible </a>
						</li>
					</ul>
					<p>
						Dentro de la Asociación existen las siguientes clases de socios:
					</p>

					<ul class="rectangle-list">
						<li>
							<a href="javascript:void(0)"> Socios/as fundadores/as - Son aquellos que participaron en el acto de constitución de la Asociación. </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Socios/as adherentes - Son los que adhieren a los valores de la Asociación y son beneficiarios de los servicios de esta. </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Socios/as activos/as - Son los que participan activamente a las actividades y gestión de la Asociación. </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Socios/as benefactores - Son las personas que apoyan financieramente o que envían regularmente donaciones a la Asociación. En este último caso, el título de benefactor es honorífico; no confiere derechos especiales. </a>
						</li>
						<li>
							<a href="javascript:void(0)"> Socios/as honoríficos/as - Son los que por su prestigio o por haber contribuido de modo relevante a la dignificación y desarrollo de la Asociación, son acreedores a tal distinción. El nombramiento de las socios/as honoríficos/as corresponderá a la Asamblea General. </a>
						</li>
					</ul>
					<hr>

				</div>
				<div class="tab-pane fade" id="como-ser">
					<h1 class="page-header">¿Cómo der Miembro de AMVIAC?</h1>
					<p>
						La membresía de AMVIAC te da la oportunidad de intercambiar con personas de otros horizontes sobre temáticas de actualidad, dándote la posibilidad de expresar tus ideas, debatir sobre temas de interés, reflexionar sobre el presente y futuro del país y de la comunidad, integrarte a un grupo de personas interesadas en contribuir a la transformación de nuestra sociedad y proponer acciones que permitan poner en marcha las ideas de la asociación.
					</p>
					<p>
						Además te ofrece la posibilidad de participar en proyectos como voluntario tanto en México como en el extranjero, de integrarte a las instancias gestoras de la asociación, vincular a AMVIAC con proyectos de interés general, presentar y representar a la asociación en diferentes foros, eventos y reuniones nacionales e internacionales, ser líder en alguno de los campamentos organizados por la asociación.
					</p>
					<p>
						Finalmente, ser miembro de AMVIAC te permite poner en valor tus capacidades de comunicación, tu experiencia en algún ámbito específico, proponer acciones que vayan con las prioridades de la institución, así como involucrarte en el quehacer de la organización.

					</p>
					<blockquote>
						<p>

							Si quieres ser miembro de AMVIAC te invitamos a llenar tu solicitud en línea. Una vez llenada, tienes que cubrir tu cuota anual y enviarnos tu formulario de membresía por correo electrónico a
							<a href="mailto:contact.amviac@gmail.com." style="color: #9c0101">contact.amviac@gmail.com.</a>

						</p>
						<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title" id="myModalLabel">Ficha de membrecia</h4>
									</div>
									<div class="modal-body">

									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">
											Cerrar
										</button>
									</div>
								</div>
							</div>
						</div>

						<p>
							AMVIAC se reserva el derecho de aceptación. En caso de que tu aplicación no obtenga una nota favorable, tu cuota te será rembolsada íntegramente.

						</p>
					</blockquote>
					<!--                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal2">-->
					<!--                        Ficha de membrecia-->
					<!--                    </button>-->
					<a type="button" class="btn btn-primary btn-lg" href="ficha.php">Ficha Membresía 2015</a>

					<hr>

				</div>
			</div>
		</div>

	</section>
	<!-- /#ccr-contact-form -->

</section><!-- /.col-md-8 / #ccr-left-section -->
<!--<section  id="ccr-right-section" class="col-md-4">-->
<!--    <form action="index.php" method="post">-->
<!--        <div class="control-group">-->
<!--            <label for="busqueda">Busqueda-->
<!--                <input type="text" name="busqueda" id="busqueda" required/>-->
<!--            </label>-->
<!--        </div>-->
<!--    </form>-->
<!---->
<!--    <section id="ccr-find-on-fb">-->
<!--        <div class="find-fb-title">-->
<!--            <span><i class="fa fa-facebook"></i></span> Encuentranos en facebook-->
<!--        </div>-->
<!--        <div class="find-on-fb-body">-->
<!---->
<!--            <div class="fb-like-box" data-href="https://www.facebook.com/amviac/" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>-->
<!---->
<!--        </div>-->
<!--    </section>-->

</section>
<?php
if ($titulo == "Inicio") {
	include "includes/footer3.php";
} else {
	include "includes/footer2.php";
}
?>

<script>
	/* FOR DEMO ONLY (NOT NECESSARY TO IMPLEMENT VERTICAL TABS) */

	var $tabSideNav = $('.navbar .navbar-nav');
	var $tabSideToggles = $('a', $tabSideNav);
	var $tabTypeContainer = $('.tabbable');
	var $tabContentContainer = $('.tab-content', $tabTypeContainer);

	if ($tabTypeContainer.hasClass('tabs-left')) {
		$tabSideToggles.filter('[data-target=left]').parent('li').addClass('active');
	} else {
		$tabSideToggles.filter('[data-target=right]').parent('li').addClass('active');
	}

	$tabSideToggles.on('click', function(e) {
		e && e.preventDefault();

		$tabSideNav.find('li').removeClass('active');
		$(this).parent('li').addClass('active');

		var target = $(this).attr('data-target');
		if (target === 'right') {
			if ($tabTypeContainer.hasClass('tabs-left')) {
				$tabTypeContainer.removeClass('tabs-left').addClass('tabs-right');
			}
		}
		if (target === 'left') {
			if ($tabTypeContainer.hasClass('tabs-right')) {
				$tabTypeContainer.removeClass('tabs-right').addClass('tabs-left');
			}
		}
	});

</script>