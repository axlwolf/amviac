<?php

include "includes/funciones.php";
$titulo = "Actividades";

include ('includes/header3.php');
?>
<link rel="stylesheet" type="text/css" href="css/modal-abd.css" />
<link rel="stylesheet" href="css/reveal.css">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal1.js"></script>
<script type="text/javascript" src="js/js.js"></script>
<body onload="itemSize();">
<section id="ccr-left-section" class="col-md-12">
	<div class="current-page">
		<a href="index.php"><i class="fa fa-home"></i> <i class="fa fa-angle-double-right"></i></a> <?= $titulo ?>
	</div>
	<!-- / .current-page -->

	<!-- BreadCrumb -->
	<div class="body-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="margin-bottom: 300px">
					<section class="callout-content">
						<div id="content" style="position:absolute; width:600px; left:550px; top:70px;"></div>
					</section>

					<div id="myModal" class="reveal-modal"></div>

				</div>
			</div>
		</div>
	</div>

</section>
</body>

<?php
if ($titulo == "Inicio") {
	include "includes/footer3.php";
} else {
	include "includes/footer2.php";
}
?>