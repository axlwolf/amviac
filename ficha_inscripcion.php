<?php
session_start();
ini_set("memory_limit", "128M");

// Iniciamos el buffer
ob_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Ficha de Membrecia AMVIAC 2015</title>
		<link rel="stylesheet" href="style.css" media="all" />
	</head>
	<body style="margin: 0 auto">

		<header>

			<div class="container-fluid">
				<div class="col-xs-12 col-md-12 logo" style="text-align:center">
					<img src="logo-sitio.png">
				</div>
			</div>

		</header>

		<div class="container-fluid" class="border: 1px solid black">

			<div class="row">

				<table class="table table-stripped " border="1">
					<tr>
						<td colspan="4" style="text-align:center;font-size:16px; color:#2980b9">
						<p>
							Formulario de inscripción
						</p>
						<p>
							Campamentos internacionales
						</p>
						<p>
							VOLUNTEER EXCHANGE FORM
						</p>
						<p>
							International workcamps
						</p></td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:center;font-size:24px"> Datos generales </td>
					</tr>
					<tr>
						<td><label for="nombre">Nombre</label></td>
						<td>
						<input type="text" id="nombre" name="nombre" required/>
						</td>
						<td><label for="apellido_paterno"></label>
						<input type="text" id="apellido_paterno" name="apellido_paterno" placeholder="Apellido Paterno" required/>
						</td>
						<td><label for="apellido_materno"></label>
						<input type="text" id="apellido_materno" name="apellido_materno" placeholder="Apellido Materno" required/>
						</td>
					</tr>
					<tr>
						<td><label for="experiencia"> Sexo: </label></td>
						<td colspan="3"><!--                  <input type="text" id="experiencia" name="experiencia"/>
						-->
						<input type="radio" name="sexo" id="sexo" value="hombre" />
						Hombre
						<input type="radio" name="sexo" id="sexo" value="mujer" />
						Mujer </td>
					</tr>
					<tr>
						<td><label for="ciudad"> Ciudad</label></td>
						<td>
						<input type="text" id="ciudad" name="ciudad" required/>
						</td>
						<td><label for="postal">Código postal</label></td>
						<td>
						<input type="text" id="postal" name="postal" required/>
						</td>
					</tr>
					<tr>
						<td><label for="estado"> Estado:</label></td>
						<td colspan="3">
						<input type="text" id="estado" name="estado" required/>
						</td>
					</tr>
					<tr>
						<td><label for="fecha_nac">Nacimiento</label></td>
						<td> año / mes / día
						<br>
						<input type="text" id="fecha_nac" name="fecha_nac" placeholder="aaaa/mm/dd"/ required>
						</td>
						<td><label for="lugar_nac">Lugar de nacimiento</label></td>
						<td>
						<input type="text" id="lugar_nac" name="lugar_nac" required/>
						</td>
					</tr>
					<tr>
						<td><label for="telefono_fijo"> Teléfono fijo </label></td>

						<td>
						<input type="tel" id="telefono_fijo" name="telefono_fijo" required/>
						</td>
						<td><label for="telefono_celular"> Teléfono celular </label></td>

						<td>
						<input type="tel" id="telefono_celular" name="telefono_celular" required/>
						</td>
					</tr>

					<tr>
						<td><label for="nacionalidad">Nacionalidad</label></td>
						<td>
						<input type="text" id="nacionalidad" name="nacionalidad"/>
						</td>

						<td><label for="experiencia"> Experiencia en el voluntariado </label></td>
						<td colspan="3"><!--                  <input type="text" id="experiencia" name="experiencia"/>
						-->
						<input type="radio" name="experiencia" id="experiencia"/>
						Si
						<input type="radio" name="experiencia" id="experiencia"/>
						No </td>
					</tr>
					<tr>
						<td><label for="email"> Email </label></td>
						<td>
						<input type="email" id="email" name="email" required/>
						</td>
						<td><label for="profesion"> Profesión / Ocupación </label></td>
						<td>
						<input type="text" id="profesion" name="profesion" required/>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align:center;font-size:16px"> Contacto en caso de emergencia </td>
					</tr>

					<tr>
						<td colspan="2">
						<input type="text" name="nombre_contacto" id="nombre_contacto" placeholder="Nombre">
						</td>
						<td>
						<input type="text" name="tel_contacto_1" id="tel_contacto_1" placeholder="Télefono (día)">
						</td>
						<td>
						<input type="text" name="tel_contacto_2" id="tel_contacto_2" placeholder="Télefono (noche)">
						</td>
					</tr>

					<tr>
						<td colspan="4" style="text-align:center;font-size:24px">Datos migratorios </td>
					</tr>

					<tr>
						<td><label for="pasaporte"> Pasaporte </label></td>
						<td>
						<input type="text" id="pasaporte" name="pasaporte" required/>
						</td>
						<td><label for="visa"> VISA (If required) </label></td>
						<td>
						<input type="text" id="visa" name="visa" required/>
						</td>
					</tr>

					<tr>
						<td colspan="4" style="text-align:center;font-size:24px"> Idiomas </td>
					</tr>

					<tr>
						<td><label for="idioma_1"> Hablo bien: </label></td>
						<td>
						<input type="text" id="idioma_1" name="idioma_1" required/>
						</td>
						<td><label for="idioma_2"> Hablo algo: </label></td>
						<td>
						<input type="text" id="idioma_2" name="idioma_2" required/>
						</td>
					</tr>

					<tr>
						<td colspan="2" style="text-align:center;font-size:16px"><label for="especiales">Necesidades especiales de Salud / Dieta especial </label></td>
						<td colspan="2">						<textarea name="especiales" id="especiales" cols="50" rows="5"></textarea></td>
					</tr>

					<tr>
						<td colspan="2" style="text-align:center;font-size:16px"><label for="especiales">Experiencia en voluntariado /Capacidades generales
							<br>
							(indique país, año y tipo de voluntariado) </label></td>
						<td colspan="2">						<textarea name="especiales" id="especiales" cols="50" rows="5"></textarea></td>
					</tr>
				</table>

				<table class="table table-stripped " border="1">
					<tr>
						<td colspan="4" style="text-align:center;font-size:24px"> Selección de proyectos de acuerdo a su preferencia </td>
					</tr>
					<tr>
						<td></td>
						<td>Código</td>
						<td>Nombre</td>
						<td>Fechas</td>
					</tr>
					<tr>
						<td>1</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>2</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>3</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>4</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>5</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>

				</table>

				<table class="table table-stripped " border="1">
					<tr>
						<td colspan="2" style="font-size:24px"><label for="registro_p">Registre otro proyecto si los escogidos estan llenos</label></td>
						<td>
						<input type="radio" name="registro_p" id="registro_p" required/>
						Si </td>
						<td>
						<input type="radio" name="registro_p" id="registro_p" required/>
						No </td>
					</tr>

					<tr>
						<td><label for="fechas_disponibles">Fechas disponibles</label></td>
						<td>
						<input type="date" id="fechas_disponibles" name="fechas_disponibles" required />
						</td>
						<td><label for="region_preferida">País / Región preferida</label></td>
						<td>
						<input type="text" name="region_preferida" id="region_preferida" required />
						</td>
					</tr>
				</table>

				<table class="table table-stripped " border="1">
					<tr>
						<td colspan="12" style="text-align:center;font-size:24px"><label for="tipo_proyectos">Tipo de proyectos preferidos</label>
						<br>
						<p style="font-size:16px">
							(Por favor llene con un número de acuerdo a su preferencia)
						</p></td>
					</tr>
					<tr>
						<td></td>
						<td> Arqueología </td>
						<td></td>
						<td> Renovación </td>
						<td></td>
						<td> Construcción </td>
						<td></td>
						<td>Medio ambiente</td>
						<td></td>
						<td>Discapacitados</td>
						<td></td>
						<td>Adolescentes</td>
					</tr>

					<tr>
						<td></td>
						<td> Agricultura </td>
						<td></td>
						<td> NIños </td>
						<td></td>
						<td> Ancianos </td>
						<td></td>
						<td>Patrimonio cultural</td>
						<td></td>
						<td>Cultural / Artes</td>
						<td></td>
						<td>Educación</td>
					</tr>

					<tr>
						<td colspan="6"> ¿Porqué desea participar en un proyecto de voluntariado? </td>
						<td colspan="6">						<textarea name="" id="" cols="50" rows="3"></textarea></td>
					</tr>

					<tr>
						<td colspan="6"> Remarcas generales: </td>
						<td colspan="6">						<textarea name="" id="" cols="50" rows="3"></textarea></td>
					</tr>

					<tr>
						<td colspan="6"> Anota claramente los nombres y correos electrónicos de amigos o amigas que deseen tomar parte como voluntarios en campamentos de servicio voluntario. </td>
						<td colspan="6">						<textarea name="" id="" cols="50" rows="3"></textarea></td>
					</tr>
					<tr>
						<td colspan="6"><label for="enteraste">¿Cómo te enteraste de AMVIAC?</label></td>
						<td colspan="6"><!--                  <input type="text" id="enteraste" name="enteraste"/>
						-->
						<select name="enteraste" id="enteraste">
							<option value="FaceBook">Por Facebook</option>
							<option value="amigo o conocido">Por amigo o conocido</option>
							<option value="Google">Por Google</option>
							<option value="Otras asociaciones">Por otras asociacones</option>
							<option value="Dependencia gubernamental">Por dependencia gubernamental</option>
						</select></td>
					</tr>

					<tr>
						<td colspan="12" style="text-align:center"> Acepto las condiciones de particiación de acuerdo al programa de esta organización y entiendo completamente mi responsabilidad para obtener seguro de salud durante la duración de viaje. </td>
					</tr>

					<tr>
						<td colspan="6" style="text-align:center"> Firma: _________________________ </td>
						<td colspan="6" style="text-align:center"> Fecha: _____________________________ </td>
					</tr>

				</table>

			</div>

		</div>
		<div class="container-fluid">

			<p>
				PARA LA CORRECTA INSCRIPCION DE LOS VOLUNTARIOS QUE QUIERAN PARTICIPAR EN UN CAMPAMENTO DE SERVICIO VOLUNTARIO EN MEXICO O EN EL EXTRANJERO ES NECESARIO LEER Y COMPRENDER ENTERAMENTE LAS SIGUIENTES CLÁUSULAS:
			</p>
			<ol>

				<li>
					Te solicitamos QUE PIENSES BIEN antes de inscribirte a al/los campamento/s de servicio voluntario, si es que tendrás el tiempo y disponibilidad para tomar parte en el/ellos.
				</li>

				<li>
					Otra opción que se te brinda es que si has desistido y has cubierto tu donativo, busques a otra persona que ocupe tu lugar, esto lo hacemos con el objeto de que no se pierda el donativo que has otorgado, de esta manera, la persona que ocupe tu lugar te cubrirá el dinero del campamento, ya que el donativo de inscripción, no es reembolsable.
				</li>

				<li>
					Si por alguna razón, cambias de campamento, habrá un costo administrativo de 50 pesos  adicionales para un campamento de fin de semana y de 300 pesos para un campamento de 2 o 3 semanas.
				</li>

				<li>
					Una vez hecho el registro de campamento/s de servicio voluntario y haber efectuado el donativo de inscripción a AMVIAC, este no será reembolsable.
				</li>

				<li>
					Ten en cuenta que no son vacaciones, es un campamento de servicio voluntario en donde podrás conocer gente de otros lugares y así mismo realizar actividades de tiempo libre y efectuar actividades de tipo social, educativo, de medio ambiente o ecológico. Las actividades a realizar dependen de cada proyecto.
				</li>

				<li>
					El voluntario es responsable de sí mismo y de sus actos. AMVIAC  no se hace responsable de los actos de los voluntarios y las consecuencias de los mismos, durante su viaje, proyecto de servicio voluntario y estancia en México o en el extranjero.
				</li>

				<li>
					AMVIAC no es responsable si el voluntario no llama por teléfono, escribe un correo electrónico avisando a su familia de que está bien o en tal lugar. Esa es responsabilidad del propio voluntario.
				</li>

				<li>
					AMVIAC  no es responsable de la cancelación de campamentos de servicio voluntario cuando se presenten fenómenos naturales como: huracanes, sismos, maremotos, tornados; o cuando se presente una guerra, invasión o una emergencia nacional en el lugar en que se encuentre el voluntario.
					El voluntario deberá obtener un seguro de vida, accidentes y enfermedades, de pérdida,  etc. completo antes de salir a su proyecto de voluntariado. Es requisito indispensable.
				</li>

				<li>
					Leída la información del campamento de servicio voluntario y las circunstancias en que se llevará a cabo, es que el voluntario está de acuerdo y efectúa esta inscripción.
				</li>
			</ol>

			<p>
				* ACEPTO HABER LEIDO Y COMPRENDIDO ENTERAMENTE LAS CLAUSULAS ANTES MENCIONADAS POR PARTE DE AMVIAC
			</p>

			<p>
				NOMBRE Y FIRMA DEL VOLUNTARIO           
			</p>
      <p>
        FECHA:
      </p>

		</div>

	</body>
	<?php
	$dir = dirname(__FILE__);
	require_once ("includes/dompdf/dompdf_config.inc.php");
	$dompdf = new DOMPDF();
	$dompdf -> load_html(ob_get_clean());
	$dompdf -> render();
	$pdf = $dompdf -> output();
	ob_start();

	$filename = "inscripcion " . date('H:i:s') . ".pdf";

	file_put_contents("pdf/" . $filename, $pdf);
	$dompdf -> stream($filename);
	?>

</html>