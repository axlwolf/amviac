<?php
$titulo = "Socios";
include ('includes/header3.php');

//obtener testos sider
$query = "SELECT * FROM empresa";

//#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);

while ($texto = $resultado -> fetch_assoc()) {
	$empresa1 = $texto['empresa'];
	$somos = $texto['somos'];
	$direccion = $texto['direccion'];
	$filosofia = $texto['filosofia'];
	$eslogan = $texto['eslogan'];
}
?>
<section id="ccr-left-section" class="col-md-6" style="margin-bottom: 400px">

    <div class="current-page">
        <a href="ficha.php"><i class="fa fa-home"></i> <i class="fa fa-angle-double-right"></i></a> <?= $titulo ?>
    </div> <!-- / .current-page -->

    <section >
        <h2>
            Nacionales
        </h2>
        <div id="owl-carousel" class="owl-carousel owl-theme">

            <div class="item">
<!--                <a href="http://www.alianzafrancesa.org.mx/cuautla" target="_blank">-->
                    <div class="col-md-12">

                        <div class="thumbnail  bordered full-rounded">
                            <img class="img-thumbnail" src="logos/nacionales/atlatlahucan.png">
                        </div>

                        <div class="desc">

                            <p style="text-align: center">
                                <strong>
                                    Municipio de <br/> Atlatlahucan
                                </strong>

                            </p>

                        </div>

                    </div>
<!--                </a>-->

            </div>

            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail  bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/pcccc.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                P.C.C.C.
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/totolapan.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Municipio de <br/> Totolapan
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/trofeo.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Trofeo  a la Vida A.C.
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/tsomanotik.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>tsomanik</strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/viva.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                               Voluntariado Internacional <br>Voluntad en Acción
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/vmexico.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>Voluntarios México
                                <br>&nbsp;</strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/xochicalco.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Xochicalco
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/yz_proyectos.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">

                            <strong>
                                YZ, Proyectos de Desarrollo A.C.
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/zacualpan.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Municipio de Zacualpan de Amilpas
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
			<div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/nacionales/kiekare.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                KieKare
                            </strong>

                        </p>
                    </div>

                </div>
            </div>

            </div>

    </section> <!-- /#ccr-contact-form -->



</section><!-- /.col-md-8 / #ccr-left-section -->

<section  id="ccr-right-section" class="col-md-6" style="margin-top: 24px">
    <section id="ccr-contact-sidebar">
       <h2>
           <p>Internacionales</p>
       </h2>
        <div id="owl-carousel2" class="owl-carousel owl-theme">

            <div class="item">
<!--                <a href="http://www.alianzafrancesa.org.mx/cuautla" target="_blank">-->
                    <div class="col-md-12">

                        <div class="thumbnail  bordered full-rounded">
                            <img class="img-thumbnail" src="logos/internacionales/chiriboga.png">
                        </div>

                        <div class="desc">
                            <p style="text-align: center">
                                <strong>
                                    Fundación Proyecto <br/>
                                    Ecológico Chiriboga
                                </strong>

                            </p>

                        </div>

                    </div>
<!--                </a>-->

            </div>

            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail  bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/concordia.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Concordia
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/csetc.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                CSETC
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/ffnepal.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>Friendship Foundation Nepal</strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/fslindia.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>FSL India</strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/heritage.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                European Heritage Volunteers
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/javva.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>JAVVA</strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/libreexpresion.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Libre Expresión
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/kenya.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Kenia Voluntary Development Association
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/nice.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                NICE
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/ruchi.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Rural Centre for Human Interests
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/seeds.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Seeds Volunteering for Iceland
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/svi.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Service VolOntaire International
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/vedi.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                VEDI NAPOLI
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/vfpeace.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Volunteers For Peace
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/vietnam.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                SJ VIETNAM
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/vinepal.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Volunteers Initiative Nepal
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/vsa.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Volunteers Spirit Association
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/vya.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Vision Youth Action
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/genctur.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Gençtur
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/solidarites.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Solidarités Jeunesses
                            </strong>

                        </p>
                    </div>

                </div>
            </div>
            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/yap.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                YAP Italia
                            </strong>

                        </p>
                    </div>

                </div>
            </div>

            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/Paro_Onde.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Para Onde
                            </strong>

                        </p>
                    </div>

                </div>
            </div>

            <div class="item">
                <div class="col-md-12">
                    <div class="thumbnail bordered full-rounded">
                        <img class="img-thumbnail" src="logos/internacionales/Mexikans.png">
                    </div>
                    <div class="desc">
                        <p style="text-align: center">
                            <strong>
                                Mexikans
                            </strong>

                        </p>
                    </div>

                </div>
            </div>


        </div>

    </section> <!-- /#ccr-contact-sidebar -->
</section><!-- / .col-md-4  / #ccr-right-section -->

<?php
if ($titulo == "Inicio") {
    include "includes/footer3.php";
} else {
    include "includes/footer2.php";
}
?>
<script src="js/owl.carousel.js"></script>
<script>
    jQuery(document).ready(function() {

        var owl = jQuery("#owl-carousel");
        var owl2 = jQuery("#owl-carousel2");

        owl.owlCarousel({
            items : 4, //10 items above 1000px browser width
            itemsDesktop : [1800,4], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,2], //2 items between 600 and 0
            itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
            autoPlay : 1800
        });





        owl2.owlCarousel({
            items : 4, //10 items above 1000px browser width
            itemsDesktop : [1200,5], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3], // betweem 900px and 601px
            itemsTablet: [600,2], //2 items between 600 and 0
            itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
            autoPlay : 2500
        });


    });
</script>
