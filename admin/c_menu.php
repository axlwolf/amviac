<?php
session_start();
//include('php_conexion.php');
include "Conexion.php";

if (!$_SESSION['tipo_usu'] == 'a') {
	header('location:error.php');
}
$nombre = '';
$url = '';
//$usuario = '';
//$con = '';
if (!empty($_GET['id'])) {
	$id = $_GET['id'];
	$query = "SELECT * FROM menu WHERE id = '$id'";
	$resultado = $conexion -> query($query);
	//			$can=mysql_query("SELECT * FROM usuarios where id=$id");
	if ($dato = $resultado -> fetch_assoc()) {
		//		$con = $dato['con'];
		$nombre = $dato['nombre'];
		$url = $dato['url'];
		//		$tipo = $dato['tipo'];
		$boton = "Actualizar";
	}
} else {
	$id = '';
	$boton = "Guardar";
}
include "include/header.php";
?>
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
							<blockquote>
 <a href="menus.php" class="btn btn-info"><i class="icon-list-alt"></i> Ir a Menus</a>
 </blockquote>
<div align="center">
<form name="form1" method="post" action="">
<table width="40%" border="0">
  <tr>
    <td colspan="3">
		<?php
		if (!empty($_POST['nombre']) and !empty($_POST['url'])) {
			$nombre = $_POST['nombre'];
			$url = $_POST['url'];

			if ($boton == 'Actualizar') {
				$query = "UPDATE menu SET nombre='$nombre', url='$url' WHERE id='$id'";
				//                UPDATE `ehs2`.`menu` SET `nombre` = 'Inicio', `url` = 'indexa.php' WHERE `menu`.`id` = 1;

				$resultado = $conexion -> query($query);
				echo '<div class="alert alert-success">
						  <button type="button" class="close" data-dismiss="alert">X</button>
						  <strong>Menú!</strong> Actualizado con Exito
					</div>';
				//				exit();
			} else {
				$select_menu = "SELECT * FROM menu WHERE nombre='$nombre'";
				$resultado = $conexion -> query($select_menu);
				//			//$can = mysql_query("SELECT * FROM usuarios where usu='$usuario'");
				if (!$dato = $resultado -> fetch_assoc()) {
					if (!preg_match("/\\s/", $nombre)) {
						$sql = "INSERT INTO menu (nombre, url, estado)
									 VALUES ('$nombre','$url','s')";
						$ejecuta = $conexion -> query($sql);
						if (copy("../servicios.php", "../$url")) {
							echo "El fichero ha sido copiado\n";
						} else {
							echo "Se ha producido un error al intentar copiar el fichero\n";
						}
						$nombre = '';
						//						$tipo = '';
						//						$usuario = '';
						$url = '';
						echo '	<div class="alert alert-success">
								  <button type="button" class="close" data-dismiss="alert">X</button>
								  <strong>Menú!</strong> Guardado con Exito
								</div>';
					} else {
						echo '	<div class="alert alert-error">
									<button type="button" class="close" data-dismiss="alert">X</button>
									<strong>Error!</strong> No se permiten espacios en los nombres de menú.
								</div>';
						$nombre = '';
						$url = '';
					}
				} else {
					echo '	<div class="alert alert-error">
									<button type="button" class="close" data-dismiss="alert">X</button>
									<strong>Error!</strong> Este menú ya existe.
								</div>';
					$usuario = '';
				}
			}
		}
        ?>
    </td>
    </tr>
  <tr>
    <td width="44%"><label for="nombre"><strong>Nombre</strong></label></td>
    <td colspan="2"><input type="text" name="nombre" id="nombre" value="<?= $nombre; ?>" autocomplete="off" required></td>
  </tr>
  <tr>
    <td><label for="url"><strong>URL o archivo</strong></label></td>
    <td colspan="2"><input type="text" name="url" id="url" value="<?= $url; ?>" autocomplete="off" required></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
      <button type="submit" class="btn btn-info"><i class="icon-ok"></i> <?=  $boton; ?></button>    </td>
    <td width="56%">
      <?php
	if (!$boton == 'Actualizar') {
		echo '<button class="btn btn-info"><a href="c_menu.php"><i class="icon-ok"></i> Ingresar Nuevo</a></button>';
	}
 ?>
    </td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>
</div>
                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
		
		
<?php

include "include/footer.php";
?>