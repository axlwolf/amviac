<?php
include "include/ConexionPDO.php";

include "classes/TipoProyecto.php";

include "include/header.php";

# Seleccionamos los tipos de proyectos
$tipo_proyeco = $dbh -> query("SELECT id,nombre_proyecto FROM tipo_proyecto");
$tipos = $tipo_proyeco -> fetchAll(PDO::FETCH_CLASS, 'TipoProyecto');

# Seleccionamos los paises
$paises = $dbh -> query("SELECT Countryid,Country FROM paises_carga ORDER BY Country ASC");
$select_pais = $paises -> fetchAll(PDO::FETCH_CLASS, 'Pais');

?>
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                                            <div class="container">

                        <div class="col-md-12">

                            <form class="form-horizontal" method="post" action="subir_imagen.php" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><strong>Agregar información del </strong> Campamento</h3>

                                    </div>
                                    <div class="panel-body">
                                        <p>
                                            Agregar información correspondiente a los campamentos AMVIAC
                                        </p>
                                    </div>
                                    <div class="panel-body">

                                        <div class="row">

                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="titulo">Título</label>
                                                    <div class="col-md-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                            <input type="text" class="form-control" id="titulo" name="titulo" required/>
                                                        </div>
                                                        <span class="help-block">Título del campamento, </span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="clave">Clave: </label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                            <input type="text" class="form-control" name="clave" id="clave" required/>
                                                        </div>
                                                        <span class="help-block">Clave del campamento</span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="lugar">Lugar: </label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select" name="lugar" id="lugar">
                                                            <?php
                                                            foreach($select_pais as $pais):
                                                                ?>
                                                                <option  value="<?= $pais -> Countryid ?>"><?= $pais -> Country ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                        <span class="help-block">País del campamento</span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="tipo">Tipo: </label>
                                                    <div class="col-md-9">
                                                        <select class="form-control select" name="tipo" id="tipo">

                                                            <?php
                                                            foreach($tipos as $tipo):

                                                                ?>
                                                                <option value="<?= $tipo -> id ?>"><?= $tipo -> nombre_proyecto ?></option>

                                                                <?php
                                                            endforeach;
                                                            ?>
                                                        </select>
                                                        <span class="help-block">Tipo del campamento</span>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="fecha">Fecha Inicio: </label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            <input type="text" id="fecha" name="fecha" class="form-control datepicker" required/>
                                                        </div>
                                                        <span class="help-block">Fecha de inicio del campamento</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="fecha2">Fecha Final: </label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            <input type="text" id="fecha2" name="fecha2" class="form-control datepicker" required/>
                                                        </div>
                                                        <span class="help-block">Fecha final del campamento</span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="descripcion">Descripción: </label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <textarea class="form-control" rows="5" id="descripcion" name="descripcion"></textarea>
                                                        <span class="help-block">Descripción breve del proyecto</span>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="observaciones">Extras / Observaciones: </label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-exclamation-triangle"></span></span>
                                                            <input type="text" id="localidad" name="extras" class="form-control" required/>
                                                        </div>
                                                        <span class="help-block">Información extra sobre el proyecto.</span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="imagen_1">Imagen 1: </label>
                                                    <div class="col-md-9">
                                                        <input type="file" class="fileinput btn-primary" id="imagen_1" name="image[]" title="Buscar imagen..." required />
                                                        <span class="help-block">Subir imagen 1 del proyecto.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="imagen_2">Imagen 2: </label>
                                                    <div class="col-md-9">
                                                        <input type="file" class="fileinput btn-primary" id="imagen_1" name="image[]" title="Buscar imagen..." required />
                                                        <span class="help-block">Subir imagen 2 del proyecto.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="imagen_3">Imagen 3:</label>
                                                    <div class="col-md-9">
                                                        <input type="file" class="fileinput btn-primary" id="imagen_1" name="image[]" title="Buscar imagen..." required />
                                                        <span class="help-block">Subir imagen 3 del proyecto.</span>
                                                    </div>
                                                </div>
                                            

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="alojamiento">Alojamiento: </label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <textarea class="form-control" rows="5" id="alojamiento" name="alojamiento"></textarea>
                                                        <span class="help-block">Información referente a el alojamiento disponible.</span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="localidad">Localidad: </label>
                                                    <div class="col-md-9 col-xs-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
                                                            <input type="text" id="localidad" name="localidad" class="form-control" required/>
                                                        </div>
                                                        <span class="help-block">Lugar especifico del proyecto</span>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>

                                    </div>
                                    <div class="panel-footer">
                                        <input class="btn btn-primary pull-right" type="submit" name="enviar_info" value="Enviar" />
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
		
		
<?php

include "include/footer.php";

?>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
<script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<!-- END THIS PAGE PLUGINS -->