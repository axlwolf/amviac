<?php
include "include/ConexionPDO.php";

$imagenes = moveFiles($_FILES);
$imagen_1 = 'noticias/' . $imagenes[0];
$imagen_2 = 'noticias/' . $imagenes[1];
$imagen_3 = 'noticias/' . $imagenes[2];
$imagen_4 = 'noticias/' . $imagenes[3];



$array_imagenes = array('img_1' => $imagen_1, 'img_2' => $imagen_2, 'img_3' => $imagen_3, 'img_4' => $imagen_4); 



if (!empty($_FILES)) {
    $existingFile = false;
    //Comprobamos que por lo menos haya un archivo
    foreach ($_FILES as $uploadedFile => $dataFile) {
        for ($i = 0; $i < count($dataFile["name"]); $i++) {
            if ($dataFile["name"][$i] != "") {
                $existingFile = true;
            };
        }
    }
    if ($existingFile) {
        //llamamos a la funcion que mueve y comprueba los archivos
        $filesUploaded = moveFiles($_FILES);
    } else {
        die("No hay un archivo para procesar");
    }
} else {
    die("No se enviaron archivos");
}


// if (isset($filesUploaded) and !empty($filesUploaded)) {
    // echo "Archivos cargados :)", "<br>";
    // foreach ($filesUploaded as $singleFile) {
        // echo $singleFile,
        // '<br>',
            // '<img src="upload/' . $singleFile . '" width="30%">',
        // '<br>',
        // '<hr>';
    // }
// }

function moveFiles($files)
{
    $inputFileName = "archivo"; //nombre del Input origen (ejemplo name="archivo[]" --tomar solo--> archivo
    $uploadDirectory = "noticias"; //ubicacion y nombre del directorio donde se guarda
    $fileLocations = array();
    $validExtensions = array('gif', 'jpg', 'jpe', 'jpeg', 'png'); //extensiones permitidas

    if (file_exists($uploadDirectory) && is_writable($uploadDirectory)) { //comprueba si el directorio existe y si es posible escribir
        if (isset($files[$inputFileName]["error"])) {
            foreach ($files[$inputFileName]["error"] as $key => $error) {
                if ($error == 0) {
                    $pieces = explode(".", $files[$inputFileName]["name"][$key]); //obtenemos la extensión
                    $extension = strtolower(end($pieces)); //la pasamos a minuscula

                    $validFileExtension = false;
                    foreach ($validExtensions as $type) { //comprobamos que sea una extensión permitida
                        if ($type == $extension) {
                            $validFileExtension = true;
                        }
                    }

                    if ($validFileExtension) { //si el archivo es valido lo intentamos mover
                        $fileName = date("Ymd") . "_" . date("is") . "_img_" . $pieces[0] . "." . $extension; //generamos un nombre personalizable
                        $currentLocation = $files[$inputFileName]["tmp_name"][$key]; //ubicacion original y temporal del archivo
                        if (!move_uploaded_file($currentLocation, "$uploadDirectory/$fileName")) {
                            // echo "No se puede mover el archivo \n";
                        } else {
                            $fileLocations[] = $fileName;
                        }
                    } else {
                        echo "Extension de archivo no valida \n";
                    }
                } else {
                    if ($error != 0 and $error != 4) { //Si no subieron archivos No enviar ninguna advertencia
                        $errorMessage = getErrorMessage($files[$inputFileName]["error"][$key]);
                        echo $errorMessage . " Intente nuevamente. \n";
                        die;
                    }
                }
            }
            return $fileLocations;
        } //fin del existe error
        else {
            echo "Uno de los archivos sobrepasa la capacidad establecida por el servidor";
        }
    } else {
        echo "No existe la carpeta para subir archivos o no tiene los permisos suficientes.";
    }
} // Termina la funcion

function getErrorMessage($error_code)
{ //Mensajes Personalizados
    switch ($error_code) {
        case UPLOAD_ERR_INI_SIZE:
            return 'El archivo excede el limite permitido por la directiva de PHP';
        case UPLOAD_ERR_FORM_SIZE:
            return 'El archivo excede el limite permitido por la directiva de HTML';
        case UPLOAD_ERR_PARTIAL:
            return 'El archivo solo se subio parcialmente, intentelo de nuevo';
        case UPLOAD_ERR_NO_FILE:
            return 'No hay archivo que subir';
        case UPLOAD_ERR_NO_TMP_DIR:
            return 'El folder temporal no existe';
        case UPLOAD_ERR_CANT_WRITE:
            return 'No tiene permisos para grabar archivos en el disco';
        case UPLOAD_ERR_EXTENSION:
            return 'El archivo tiene una extensión NO permitida';
        default:
            return 'Error desconocido al subir el archivo';
    }
} // Termina funcion mensajesErrorArchivos

// exit();
if (isset($_POST['enviar_info'])) {

    $date = new DateTime($campamento->fecha); // Fecha actual
    $date2 = new DateTime($campamento->fecha2); // segunda fecha
    $duracion = "";

    $date->setTimeZone( new DateTimeZone('America/Mexico_City') ); // Definimos seTimeZone para asegurarnos de que sea la hora actual del lugar donde estamos

    $interval = $date->diff( $date2 ); // Restamos la Fecha1 menos la Fecha2

    $tiempo = $interval->format('%a');
    //var_dump($tiempo);
    $tiempo_2 = (int)$tiempo;
    //var_dump($tiempo_2);

    if($tiempo_2 <= 14 and $tiempo_2 <= 21){
        $duracion = "corto";
    }elseif($tiempo_2 > 21 and $tiempo_2 <= 150){
        $duracion = "mediano";
    }else{
        $duracion = "largo";
    }


// exit();
    # Preparamos la query SQL
    $stmt = $dbh -> prepare("INSERT INTO campamentos(titulo_proyecto,clave_proyecto,fecha,fecha2,descripcion,alojamiento,localidad,observaciones,imagen_1,imagen_2,imagen_3,imagen_4,id_lugar,id_tipo_proyecto,duracion,estado)
                          VALUES (
                          :titulo,
						  :clave,
						  :fecha,
						  :fecha2,
                          :descripcion,
						  :alojamiento,
                          :localidad,
						  :extras,
						  :imagen_1,
                          :imagen_2,
						  :imagen_3,
						  :imagen_4,
                          :lugar,
						  :tipo,
						  :duracion,
						  's'
                          )");

    # Ejecutamos la query
    if ($stmt -> execute(array(
	
		// $imagen_1 = 'noticias/' . $imagenes[0];
		// $imagen_2 = 'noticias/' . $imagenes[1];
		// $imagen_3 = 'noticias/' . $imagenes[2];
		
        'titulo'      =>$_POST['titulo'],
        'clave'       =>$_POST['clave'],
        'fecha'		  =>$_POST['fecha'],
        'fecha2' 	  =>$_POST['fecha2'],
        'descripcion' =>$_POST['descripcion'],
        'alojamiento' =>$_POST['alojamiento'],
        'localidad'   =>$_POST['localidad'],
        'extras'      =>$_POST['extras'],
        'imagen_1'	  =>$array_imagenes['img_1'],
        'imagen_2'    =>$array_imagenes['img_2'],
        'imagen_3'    =>$array_imagenes['img_3'],
        'imagen_4'    =>$array_imagenes['img_4'],
        'lugar' 	  =>$_POST['lugar'],
        'tipo' 		  =>$_POST['tipo'],
        'duracion'    =>$duracion

    ))) {
        # Si se ejecuto exitosamente la query regresamos a index.php
        header("Location: campamentos.php");
    } else {

        # Nos muestra un error
        echo "<p>No se ha podido agregar la informaci�n</p>";
    }
} else {

    #Creamos la variable para tener acceso en nuestro formulario
    $campamentos = new Campamento;
}//var_dump($tipos);