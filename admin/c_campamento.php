<?php
session_start();

include "include/ConexionPDO.php";
include "classes/TipoProyecto.php";

include "Conexion.php";


if (!$_SESSION['tipo_usu'] == 'a')
    header('location:index.php');


# Seleccionamos los tipos de proyectos
$tipo_proyeco = $dbh -> query("SELECT id,nombre_proyecto FROM tipo_proyecto");
$tipos = $tipo_proyeco -> fetchAll(PDO::FETCH_CLASS, 'TipoProyecto');

# Seleccionamos los paises
$paises = $dbh -> query("SELECT Countryid,Country FROM paises_carga");
$select_pais = $paises -> fetchAll(PDO::FETCH_CLASS, 'Pais');

$id = $_GET['id'];

$query = "SELECT
            titulo_proyecto,
            clave_proyecto,
            fecha,
            descripcion,
            alojamiento,
            localidad,
            observaciones,
            imagen_1,
            imagen_2,
            imagen_3,
            imagen_4,
            id_lugar,
            id_tipo_proyecto,
            estado,
            tipo_proyecto.nombre_proyecto,
            paises_carga.Country,
            paises_carga.CountryId
            FROM campamentos
            INNER JOIN paises_carga,tipo_proyecto
            WHERE paises_carga.Countryid = campamentos.id_lugar
            AND tipo_proyecto.id = campamentos.id_tipo_proyecto
            AND campamentos.id_camp = $id
            ";
#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);


#Checamos si el id fue enviado por el metodo GET
if(!isset($_GET['id']))
    header("Location: campamentos.php");



//#El formulario de update ha sido enviado
if($_POST['enviar_info']) {

    # TODO: Corregir las opciones de update
    $datos = array($_POST);

    var_dump($datos);

    $id = $_GET['id'];

    $titulo         = $datos['titulo'];
    $clave          = $datos['clave'];
    $fecha          = $datos['fecha'];
    $descripcion    = $datos['descripcion'];
    $alojamiento    = $datos['alojamiento'];
    $localidad      = $datos['localidad'];
    $extras         = $datos['extras'];
    $imagen_1       = $datos['imagen_1'];
    $imagen_2       = $datos['imagen_2'];
    $imagen_3       = $datos['imagen_3'];
    $imagen_4       = $datos['imagen_4'];
    $lugar          = $datos['lugar'];
    $tipo           = $datos['tipo'];

//    var_dump($_POST);

//    print_r($_POST['proyecto']);

    $query = "UPDATE campamentos
              SET
                titulo_proyecto  = $titulo,
                clave_proyecto   = $clave,
                fecha            = $fecha,
                descripcion      = $descripcion,
                alojamiento      = $alojamiento,
                localidad        = $localidad,
                observaciones    = $extras,
                imagen_1         = $imagen_1,
                imagen_2         = $imagen_2,
                imagen_3         = $imagen_3,
                imagen_4         = $imagen_4,
                id_lugar         = $lugar,
                id_tipo_proyecto = $tipo
                WHERE id = $id

              ";
    $statement = $conexion->query($query);

    if($statement){
        print 'Success! record updated';
    }else{
        print 'Error : ('. $conexion->errno .') '. $conexion->error;
    }
}

include "include/header.php";
?>
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

	<div class="row">
		<div class="col-md-12">
		  <?php
while($campamento = $resultado->fetch_assoc()):
//                  var_dump($campamento);
    ?>

                 <form class="form-horizontal" method="post" action="">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><strong>Modificar información de</strong> Campamento</h3>

                                </div>
                                <div class="panel-body">
                                    <p>
                                        Modificar la información correspondiente a los campamentos AMVIAC
                                    </p>
                                </div>
                                <div class="panel-body">

                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="titulo">Título</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control" id="titulo" name="proyecto[titulo]" value="<?=  $campamento['titulo_proyecto'] ?>" required/>
                                                    </div>
                                                    <span class="help-block">Título del campamento, </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="clave">Clave: </label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                        <input type="text" class="form-control" name="proyecto[clave]" id="clave" value="<?=  $campamento['clave_proyecto'] ?>" required/>
                                                    </div>
                                                    <span class="help-block">Clave del campamento</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="lugar">Lugar: </label>
                                                <div class="col-md-9">
                                                    <select class="form-control select" name="proyecto[lugar]" id="lugar">
                                                        <?php
foreach($select_pais as $pais):
    ?>
    <option  value="<?= $pais -> Countryid ?>"><?= $pais -> Country ?></option>
<?php
endforeach;
?>
                                                        <option value="<?=  $campamento['id_lugar'] ?>" selected><?=  $campamento['Country'] ?></option>
                                                    </select>
                                                    <span class="help-block">País del campamento</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="tipo">Tipo: </label>
                                                <div class="col-md-9">
                                                    <select class="form-control select" name="proyecto[tipo]" id="tipo">

                                                        <?php
foreach($tipos as $tipo):

    ?>
    <option value="<?= $tipo -> id ?>"><?= $tipo -> nombre_proyecto ?></option>

<?php
endforeach;
?>
                                                        <option value="<?=  $campamento['id_tipo_proyecto'] ?>" selected><?=  $campamento['nombre_proyecto'] ?></option>

                                                    </select>
                                                    <span class="help-block">Tipo del campamento</span>
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="fecha">Fecha: </label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        <input type="text" id="fecha" name="proyecto[fecha]" class="form-control datepicker"  value="<?=  $campamento['fecha'] ?>"/>
                                                    </div>
                                                    <span class="help-block">Fecha de inicio del campamento</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="descripcion">Descripción: </label>
                                                <div class="col-md-9 col-xs-12">
                                                    <textarea class="form-control" rows="5" id="descripcion" name="proyecto[descripcion]">
                                                       <?=  $campamento['descripcion'] ?>
                                                    </textarea>
                                                    <span class="help-block">Descripción breve del proyecto</span>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="observaciones">Extras / Observaciones: </label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-exclamation-triangle"></span></span>
                                                        <input type="text" id="localidad" name="proyecto[extras]" class="form-control" value="<?=  $campamento['observaciones'] ?>"/>
                                                    </div>
                                                    <span class="help-block">Información extra sobre el proyecto.</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="imagen_1">Imagen 1: </label>
                                                <div class="col-md-9">
                                                    <input type="file" class="fileinput btn-primary" name="proyecto[imagen_1]" id="imagen_1" title="Buscar imagen..."/>
                                                    <span class="help-block">Subir imagen 1 del proyecto.</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="imagen_2">Imagen 2: </label>
                                                <div class="col-md-9">
                                                    <input type="file" class="fileinput btn-primary" name="proyecto[imagen_2]" id="imagen_2" title="Buscar imagen..."/>
                                                    <span class="help-block">Subir imagen 2 del proyecto.</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="imagen_3">Imagen 3: </label>
                                                <div class="col-md-9">
                                                    <input type="file" class="fileinput btn-primary" name="proyecto[imagen_3]" id="imagen_3" title="Buscar imagen..."/>
                                                    <span class="help-block">Subir imagen 3 del proyecto.</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="imagen_4">Imagen 4: </label>
                                                <div class="col-md-9">
                                                    <input type="file" class="fileinput btn-primary" name="proyecto[imagen_4]" id="imagen_4" title="Buscar imagen..."/>
                                                    <span class="help-block">Subir imagen 4 del proyecto.</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="alojamiento">Alojamiento: </label>
                                                <div class="col-md-9 col-xs-12">
                                                    <textarea class="form-control" rows="5" id="alojamiento" name="proyecto[alojamiento]">
                                                        <?=  $campamento['alojamiento'] ?>
                                                    </textarea>
                                                    <span class="help-block">Información referente a el alojamiento disponible.</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="localidad">Localidad: </label>
                                                <div class="col-md-9 col-xs-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
                                                        <input type="text" id="localidad" name="proyecto[localidad]" class="form-control" value="<?=  $campamento['localidad'] ?>"/>
                                                    </div>
                                                    <span class="help-block">Lugar especifico del proyecto</span>
                                                </div>
                                            </div>

<!
                                        </div>

                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <button class="btn btn-primary pull-right" type="submit" name="enviar_info">Modificar</button>
                                </div>
                            </div>
                            </form>


<?php
endwhile
?>
		</div>
	</div>

</div>
<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<?php

include "include/footer.php";
?>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
<script type="text/javascript" src="js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<!-- END THIS PAGE PLUGINS -->