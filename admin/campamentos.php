<?php
session_start();
include "include/ConexionPDO.php";
include "classes/TipoProyecto.php";

include ('php_conexion.php');
include "Conexion.php";

if (!$_SESSION['tipo_usu'] == 'a') {
	header('location:index.php');
} else {
	if (!empty($_GET['estado'])) {
		$codig = $_GET['estado'];
		//$cans=mysql_query("SELECT * FROM menu WHERE estado='s' and id=$codig");
		$query = "SELECT * FROM campamentos WHERE estado='s' AND id=$codig";
		$resultado = $conexion -> query($query);
		//if($d=mysql_fetch_array($cans)){
		if ($d = $resultado -> fetch_assoc()) {
			$estado = 'n';
		} else {
			$estado = 's';
		}
		$query = "UPDATE campamentos SET estado='$estado' WHERE id=$codig";
		$resultado = $conexion -> query($query);
	}

}

$query = "SELECT
            campamentos.id_camp,titulo_proyecto,clave_proyecto,fecha,descripcion,
            alojamiento,localidad,observaciones,imagen_1,
            imagen_2,imagen_3,imagen_4,tipo_proyecto.nombre_proyecto,paises_carga.Country
 FROM campamentos INNER JOIN paises_carga,tipo_proyecto WHERE paises_carga.Countryid = campamentos.id_lugar AND tipo_proyecto.id = campamentos.id_tipo_proyecto";
#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);
//$campamento = $resultado->fetch_assoc();
//var_dump($campamento);

include "include/header.php";
?>
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

	<div class="row">
		<div class="continer">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Campamentos AMVIAC</h3>
						<div class="btn-group pull-right">
							<button class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-bars"></i> Exportar Datos
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false'});"><img src='img/icons/json.png' width="24"/> JSON</a>
								</li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false',ignoreColumn:'[2,3]'});"><img src='img/icons/json.png' width="24"/> JSON (ignoreColumn)</a>
								</li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'true'});"><img src='img/icons/json.png' width="24"/> JSON (with Escape)</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'xml',escape:'false'});"><img src='img/icons/xml.png' width="24"/> XML</a>
								</li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'sql'});"><img src='img/icons/sql.png' width="24"/> SQL</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='img/icons/csv.png' width="24"/> CSV</a>
								</li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='img/icons/txt.png' width="24"/> TXT</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a>
								</li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='img/icons/word.png' width="24"/> Word</a>
								</li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img src='img/icons/ppt.png' width="24"/> PowerPoint</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'png',escape:'false'});"><img src='img/icons/png.png' width="24"/> PNG</a>
								</li>
								<li>
									<a href="#" onClick ="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img src='img/icons/pdf.png' width="24"/> PDF</a>
								</li>
							</ul>
						</div>

					</div>
					<div class="panel-body">
						<table id="customers2" class="table datatable">
							<thead>
								<tr>
									<th><strong> Clave proyecto </strong></th>

									<th><strong> <strong>Nombre</strong> </strong></th>

									<th><strong> Fecha</strong> </th>

<!--									<th><strong> Descripción </strong></th>-->
<!---->
<!--									<th><strong> Alojamiento </strong></th>-->
<!---->
<!--									<th><strong> Localidad </strong></th>-->
<!---->
<!--									<th><strong> Extras/Observaciones</strong></th>-->
<!---->
<!--									<th><strong> Imagen 1</strong></th>-->
<!---->
<!--									<th><strong> Imagen 2</strong></th>-->
<!---->
<!--									<th><strong> Imagen 3</strong></th>-->
<!---->
<!--									<th><strong> Imagen 4</strong></th>-->

									<th><strong> Lugar</strong></th>

									<th><strong> Tipo</strong></th>

									<th><strong>Estado</strong></th>

									<th><strong>Acciones</strong></th>

								</tr>
							</thead>
							<tbody>
								<?php

								while($campamento = $resultado->fetch_assoc()):
//                                    var_dump($campamento);
                                    if ($campamento['estado'] == "n") {
                                        $estado = '<span class="label label-danger">Inactivo</span>';
                                    } else {
                                        $estado = '<span class="label label-success">Activo</span>';
                                    }
								?>

								<tr>
									<td>
                                        <?=  $campamento['clave_proyecto'] ?>
                                    </td>

									<td>
                                        <a href="detalle_campamento.php?id=<?= $campamento['id_camp'] ?>">
                                        <?=  $campamento['titulo_proyecto']; ?>
                                        </a>
                                    </td>

									<td>
									<?=  $campamento['fecha'] ?>

									</td>

<!--                                    <td>-->
<!--									    --><?//=  $campamento['descripcion'] ?>
<!--                                    </td>-->
<!---->
<!--									<td>-->
<!--                                        --><?//=  $campamento['alojamiento'] ?>
<!---->
<!--                                    </td>-->
<!---->
<!--									<td>-->
<!--									--><?//=  $campamento['localidad'] ?>
<!--									</td>-->
<!---->
<!--                                    <td>-->
<!--									--><?//=  $campamento['observaciones'] ?>
<!--									</td>-->
<!---->
<!--                                    <td>-->
<!--									--><?//=  $campamento['imagen_1'] ?>
<!--									</td>-->
<!---->
<!--                                    <td>-->
<!--									--><?//=  $campamento['imagen_2'] ?>
<!--									</td>-->
<!---->
<!--                                    <td>-->
<!--									--><?//=  $campamento['imagen_3'] ?>
<!--									</td>-->
<!---->
<!--                                    <td>-->
<!--									--><?//=  $campamento['imagen_4'] ?>
<!--									</td>-->

                                    <td>
									<?=  $campamento['Country'] ?>
									</td>

                                    <td>
									<?=  $campamento['nombre_proyecto'] ?>
									</td>

									<td>
                                        <a href="campamentos.php?estado=<?=$campamento['id']; ?>">
									        <?php echo $estado  ?>
                                        </a>
                                    </td>

									<td>
									<!--<a href="c_campamento.php?id=<?php echo $campamento['id_camp']; ?>" name="editar_campamento" title="Editar Campamento" class="btn btn-info"><i class="fa fa-edit"></i>-->

									</a>
									<a href="eliminar_campamento.php?id=<?php echo $campamento['id_camp']; ?>" name="eliminar" title="Eliminar Miembro" class="btn btn-info delete" data-confirm="¿Deseas eliminar la información de este campamento?"><span class="fa fa-trash-o"></span>

									</td>
								</tr>
								<?php

								endwhile;
								?>
							</tbody>
						</table>
					</div>
				</div>
				<!-- END DEFAULT DATATABLE -->

			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<?php

include "include/footer.php";
?>

<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/tableExport.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/jquery.base64.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/html2canvas.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/jspdf/jspdf.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/jspdf/libs/base64.js"></script>
<!-- END PAGE PLUGINS -->
<script>
	$('.delete').on("click", function(e) {
		e.preventDefault();

		var choice = confirm($(this).attr('data-confirm'));

		if (choice) {
			window.location.href = $(this).attr('href');
		}
	});

</script>