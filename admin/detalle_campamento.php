<?php
session_start();

include ('php_conexion.php');
include "Conexion.php";

if (!$_SESSION['tipo_usu'] == 'a')
    header('location:index.php');

$id = $_GET['id'];

$query = "SELECT *  FROM campamentos,paises_carga,tipo_proyecto WHERE paises_carga.Countryid = campamentos.id_lugar 
AND tipo_proyecto.id = campamentos.id_tipo_proyecto AND campamentos.id_camp = $id";
#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);
//$campamento = $resultado->fetch_assoc();
//var_dump($campamento);

include "include/header.php";
?>
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <!-- BLUEIMP GALLERY -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
    <!-- END BLUEIMP GALLERY -->
    <div class="row">
		<div class="col-md-12">
        <?php
              while($campamento = $resultado->fetch_assoc()):
//                  var_dump($campamento);
        ?>
                  <div class="col-md-6 col-md-offset-3">
                      <div class="panel panel-info">

                          <div class="panel-heading">
                             <div class="row">
                                 <div class="col-md-4">
                                     <img src="img/logo-sitio.png" alt="Logo AMVIAC" width="150"/>
                                 </div>
                                 <div class="col-md-4">Voluntariado en <?=  $campamento['Country'] ?></div>
                                 <div class="col-md-4">
                                     Informes <a href="mailto:contact.amviac@gmail.com">
                                         contact.amviac@gmail.com
                                     </a>
                                 </div>
                             </div>

                              <div class="row">
                                  <div class="col-md-4">
                                      <p>
                                          <?=  $campamento['clave_proyecto'] ?>
                                      </p>
                                      <p>
                                          Tipo de proyecto:  <?=  $campamento['nombre_proyecto'] ?>
                                      </p>
                                  </div>
                                  <div class="col-md-4">
                                      <?=  $campamento['titulo_proyecto'] ?>
                                  </div>
                                  <div class="col-md-4">
								  <?php
										/*$date = new DateTime($campamento['fecha']); // Fecha actual
										$date2 = new DateTime($campamento['fecha2']); // segunda fecha
										$texto = "";*/

										//$date->setTimeZone( new DateTimeZone('America/Mexico_City') ); // Definimos seTimeZone para asegurarnos de que sea la hora actual del lugar donde estamos

										/*$interval = $date->diff( $date2 ); // Restamos la Fecha1 menos la Fecha2
										
										$tiempo = $interval->format('%a');
										//var_dump($tiempo);
										$tiempo_2 = (int)$tiempo;
										//var_dump($tiempo_2);
										
										if($tiempo_2 <= 14 and $tiempo_2 <= 21){
											$texto = "Corto plazo. (De 2 a 3 semanas)";
										}elseif($tiempo_2 > 21 and $tiempo_2 <= 150){
											$texto = "Mediano plazo. (De 2 a 5 meses)";
										}else{
											$texto = "Largo plazo (6 a 12 meses)";
										}*/
										
										// echo ; // Mostramos el resultado
                    //$interval->format('%a días')
                    
                    if ($campamento['duracion']=='corto') {
                      echo $campamento['duracion'].' plazo (De 2 a 3 semanas)';
                    }elseif ($campamento['duracion']=='mediano') {
                      echo $campamento['duracion'].' plazo (De 2 a 5 meses)';

                    }elseif ($campamento['duracion']=='largo') {
                      echo $campamento['duracion'].' plazo (6 a 12 meses)';
                    }
								  ?>
                                     
                                  </div>
                              </div>
                          </div>

                          <div class="panel-body">
                              <p><span class="fa fa-caret-right"></span> Trabajo/Proyecto: <?=  $campamento['descripcion'] ?></p>
                              <p><span class="fa fa-caret-right"></span> Alojamiento: <?=  $campamento['alojamiento'] ?></p>
                              <p><span class="fa fa-caret-right"></span> Localidad: <?=  $campamento['localidad'] ?></p>
                              <p><span class="fa fa-caret-right"></span> Extras/Observaciones especiales: <?=  $campamento['observaciones'] ?></p>
                          </div>
                          <div class="panel-footer">
                              <div class="row">
                                  <div class="col-md-4">
                                      <a href="<?=  $campamento['imagen_1'] ?>" title="Nature Image 1" data-gallery="">
                                          <img src="<?=  $campamento['imagen_1'] ?>" class="img-responsive img-text" width="189" height="120">
                                      </a>
                                  </div>
                                  <div class="col-md-4">
                                      <a href="<?=  $campamento['imagen_2'] ?>" title="Nature Image 1" data-gallery="">
                                          <img src="<?=  $campamento['imagen_2'] ?>" class="img-responsive img-text" width="189" height="120">
                                      </a>
                                  </div>
                                  <div class="col-md-4">
                                      <a href="<?=  $campamento['imagen_3'] ?>" title="Nature Image 1" data-gallery="">
                                          <img src="<?=  $campamento['imagen_3'] ?>" class="img-responsive img-text" width="189" height="120">
                                      </a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  </div>
              <?php
          endwhile
          ?>





</div>
<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<?php

include "include/footer.php";
?>

<!-- START THIS PAGE PLUGINS-->
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false&amp;libraries=places"></script>
<script type="text/javascript" src="js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
<!-- END THIS PAGE PLUGINS-->