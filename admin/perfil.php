<?php
session_start();
include ('php_conexion.php');
include "Conexion.php";

if (!$_SESSION['tipo_usu'] == 'a') {
	header('location:index.php');
} else {
	if (!empty($_GET['estado'])) {
		$codig = $_GET['estado'];
		//$cans=mysql_query("SELECT * FROM menu WHERE estado='s' and id=$codig");
		$query = "SELECT * FROM miembros WHERE estado='s' AND id=$codig";
		$resultado = $conexion -> query($query);
		//if($d=mysql_fetch_array($cans)){
		if ($d = $resultado -> fetch_assoc()) {
			$estado = 'n';
		} else {
			$estado = 's';
		}
		$query = "UPDATE miembros SET estado='$estado' WHERE id=$codig";
		$resultado = $conexion -> query($query);
	}

}

$id = $_GET['id'];
include "include/header.php";
?>
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                <?php
				//obtener menus
				$query = "SELECT * FROM miembros WHERE id= '$id'";

				#Resultado
				$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);
				?>
				
                    <div class="row">
						<?php
									  while($miembro = $resultado->fetch_assoc()):
							?>
                        <div class="col-md-6 col-md-offset-3">
						
						   <div class="panel panel-default">
                                <div class="panel-body profile">
                                    <div class="profile-image">
                                        <img src="assets/images/users/avatar.png" alt="<?= $miembro['nombre'], ' ' , $miembro['apellido_paterno'], ' ' , $miembro['apellido_materno'] ?>"/>
                                    </div>
                                    <div class="profile-data">
                                        <div class="profile-data-name"><?= $miembro['nombre'], ' ' , $miembro['apellido_paterno'], ' ' , $miembro['apellido_materno'] ?></div>
                                        <div class="profile-data-title" style="color: #FFF;">
											<?=
											$miembro['tipo_socio'];
											?>
										</div>
                                        <div class="profile-data-title" style="color: #FFF;">
											<?=  $miembro['email'] ?>
										</div>
                                    </div>
                                                                  
                                </div>                                
                                <div class="panel-body">                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn btn-info btn-rounded btn-block"><span class="fa fa-check"></span> Pagado</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body list-group border-bottom">
                                    <a href="#" class="list-group-item"><span class="fa fa-bar-chart-o"></span>Tipo de pago: <span class="badge badge-default"><?=  $miembro['adjunto'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-bar-chart-o"></span>Ciudad: <span class="badge badge-default"><?=  $miembro['ciudad'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-coffee"></span> Código postal: <span class="badge badge-default"><?=  $miembro['codigo_postal'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-users"></span> Fecha de nacimiento: <span class="badge badge-danger"><?=  $miembro['fecha_nacimiento'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-folder"></span> Lugar de nacimiento: <span class="badge badge-danger"><?=  $miembro['lugar_nacimiento'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-cog"></span> Teléfono fijo: <span class="badge badge-default"><?=  $miembro['telefono_fijo'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-cog"></span> Teléfono celular: <span class="badge badge-default"><?=  $miembro['telefono_celular'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-cog"></span> Nacionalidad: <span class="badge badge-default"><?=  $miembro['nacionalidad'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-cog"></span> Email: <span class="badge badge-default"><?=  $miembro['email'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-cog"></span> Profesión: <span class="badge badge-default"><?=  $miembro['profesion'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-cog"></span> Experiencia en voluntariados: <span class="badge badge-default"><?=  $miembro['exp_voluntariado'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-cog"></span> Como se entero de AMVIAC: <span class="badge badge-default"><?=  $miembro['enteraste'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-cog"></span> Lugar: <span class="badge badge-default"><?=  $miembro['lugar'] ?></span></a>
                                    <a href="#" class="list-group-item"><span class="fa fa-cog"></span> Fecha de registro: <span class="badge badge-default"><?=  $miembro['fecha_actual'] ?></span></a>
                                </div>
                            </div>                            
                            
						
								<?php
								endwhile;
							?>
											
                        </div>	
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
		
		
<?php

include "include/footer.php";
?>