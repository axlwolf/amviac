<?php
session_start();
include ('php_conexion.php');
include "Conexion.php";

if (!$_SESSION['tipo_usu'] == 'a') {
	header('location:index.php');
} else {
	if (!empty($_GET['estado'])) {
		$codig = $_GET['estado'];
		//$cans=mysql_query("SELECT * FROM menu WHERE estado='s' and id=$codig");
		$query = "SELECT * FROM miembros WHERE estado='s' AND id=$codig";
		$resultado = $conexion -> query($query);
		//if($d=mysql_fetch_array($cans)){
		if ($d = $resultado -> fetch_assoc()) {
			$estado = 'n';
		} else {
			$estado = 's';
		}
		$query = "UPDATE miembros SET estado='$estado' WHERE id=$codig";
		$resultado = $conexion -> query($query);
	}

}
include "include/header.php";
?>
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

	<div class="row">
		<div class="container">
			<!--                        <div align="right">-->
			<!--<form name="form1" method="post" action="">-->
			<!--	<a href="c_menu.php" class="btn btn-info"><i class="icon-user"></i> Nuevo menú</a>-->
			<!--	-->
			<!--</form>-->
			<!--</div>-->

			<?php
			include "Conexion.php";

			//obtener menus
			$query = "SELECT * FROM miembros";

			#Resultado
			$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);

			#Ontenemos los menus
			//$menus = $resultado->fetch_assoc();
			?>

			<div class="page-content-wrap">

				<div class="row">
					<div class="col-md-12">
						<!-- START DEFAULT DATATABLE -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Miembros AMVIAC</h3>
								<div class="btn-group pull-right">
									<button class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-bars"></i> Exportar Datos
									</button>
									<ul class="dropdown-menu">
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false'});"><img src='img/icons/json.png' width="24"/> JSON</a>
										</li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'false',ignoreColumn:'[2,3]'});"><img src='img/icons/json.png' width="24"/> JSON (ignoreColumn)</a>
										</li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'json',escape:'true'});"><img src='img/icons/json.png' width="24"/> JSON (with Escape)</a>
										</li>
										<li class="divider"></li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'xml',escape:'false'});"><img src='img/icons/xml.png' width="24"/> XML</a>
										</li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'sql'});"><img src='img/icons/sql.png' width="24"/> SQL</a>
										</li>
										<li class="divider"></li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'csv',escape:'false'});"><img src='img/icons/csv.png' width="24"/> CSV</a>
										</li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'txt',escape:'false'});"><img src='img/icons/txt.png' width="24"/> TXT</a>
										</li>
										<li class="divider"></li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'excel',escape:'false'});"><img src='img/icons/xls.png' width="24"/> XLS</a>
										</li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'doc',escape:'false'});"><img src='img/icons/word.png' width="24"/> Word</a>
										</li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'powerpoint',escape:'false'});"><img src='img/icons/ppt.png' width="24"/> PowerPoint</a>
										</li>
										<li class="divider"></li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'png',escape:'false'});"><img src='img/icons/png.png' width="24"/> PNG</a>
										</li>
										<li>
											<a href="#" onClick ="$('#customers2').tableExport({type:'pdf',escape:'false'});"><img src='img/icons/pdf.png' width="24"/> PDF</a>
										</li>
									</ul>
								</div>

							</div>
							<div class="panel-body">
								<table id="customers2" class="table datatable">
									<thead>
										<tr>
											<th><strong> Tipo de miembro </strong></th>

											<th><strong> Adjunto </strong></th>

											<th><strong>Nombre</strong></th>
											<th><strong> Ciudad </strong></th>
											<th><strong> Codigo Postal </strong></th>
											<th><strong> Fecha de nacimiento </strong></th>
											<th><strong> Lugar de nacimiento </strong></th>

											<th><strong>Estado</strong></th>
											<th><strong>Acciones</strong></th>

										</tr>
									</thead>
									<tbody>
										<?php

while($miembro = $resultado->fetch_assoc()):
if($miembro['estado']=="n"){
$estado='<span class="label label-danger">Inactivo</span>';
}else{
$estado='<span class="label label-success">Activo</span>';
}
										?>

										<tr>
											<td> <?=  $miembro['tipo_socio'] ?> </td>
											<td> <?=  $miembro['adjunto'] ?> </td>
											<td><a href="perfil.php?id=<?= $miembro['id'] ?>"> <?= $miembro['nombre'], ' ' , $miembro['apellido_paterno'], ' ' , $miembro['apellido_materno'] ?> </a></td>
											<td> <?=  $miembro['ciudad'] ?> </td>
											<td> <?=  $miembro['codigo_postal'] ?> </td>
											<td> <?=  $miembro['fecha_nacimiento'] ?> </td>
											<td> <?=  $miembro['lugar_nacimiento'] ?> </td>
											<td>
                                                <a href="miembros.php?estado=<?= $miembro['id'];?>">
										        	<?php echo $estado  ?>
                                                </a>

                                            </td>
											<td>
<!--<a href="c_miembro.php?id=<?php //echo $miembro['id']; ?>" name="editar_miembro" title="Editar Miembro" class="btn btn-info"><i class="fa fa-edit"></i> </a>-->
<a href="eliminar_miembro.php?id=<?php echo $miembro['id']; ?>" name="eliminar" title="Eliminar Miembro" class="btn btn-info delete" data-confirm="¿Deseas eliminar a este miembro?"><span class="fa fa-trash-o"></span> </a></td>
										</tr>
										<?php

										endwhile;
										?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END DEFAULT DATATABLE -->
					</div>
				</div>

			</div>
			<!-- END PAGE CONTENT WRAPPER -->
		</div>
		<!-- END PAGE CONTENT WRAPPER -->
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<?php

include "include/footer.php";
?>

<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src='js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/tableExport.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/jquery.base64.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/html2canvas.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/jspdf/jspdf.js"></script>
<script type="text/javascript" src="js/plugins/tableexport/jspdf/libs/base64.js"></script>
<!-- END PAGE PLUGINS -->
<script>
	$('.delete').on("click", function(e) {
		e.preventDefault();

		var choice = confirm($(this).attr('data-confirm'));

		if (choice) {
			window.location.href = $(this).attr('href');
		}
	});

</script>