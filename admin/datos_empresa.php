<?php
session_start();
include ('php_conexion.php');
$mensaje = "0";
$usu = $_SESSION['username'];
if (!$_SESSION['tipo_usu'] == 'a') {
	header('location:error.php');
}
if (!empty($_POST['empresa']) and !empty($_POST['nit'])) {
	$nameimagen = $_FILES['imagen']['name'];
	$tmpimagen = $_FILES['imagen']['tmp_name'];
	$extimagen = pathinfo($nameimagen);
	$ext = array("png", "jpg");
	$urlnueva = "img/logo.png";
	if (is_uploaded_file($tmpimagen)) {
		if (array_search($extimagen['extension'], $ext)) {
			copy($tmpimagen, $urlnueva);
		}
	}
	$nempresa = $_POST['empresa'];
	$ndireccion = $_POST['direccion'];
	$ntel1 = $_POST['telefono'];
	$ntel2 = $_POST['celular'];
	$ncorreo = $_POST['correo'];
	$nweb = $_POST['web'];
	$nciudad = $_POST['ciudad'];
	$nnit = $_POST['nit'];
	$region = $_POST['region'];
	$eslogan = $_POST['eslogan'];
	$xSQL = "Update empresa Set empresa='$nempresa',
								  nit='$nnit',
								  direccion='$ndireccion',
								  ciudad='$nciudad',
								  tel1='$ntel1',
								  tel2='$ntel2',
								  web='$nweb',
								  correo='$ncorreo',
								  region='$region',
								  eslogan='$eslogan'
							Where id=1";
	mysql_query($xSQL);
	$mensaje = "1";
}

$can = mysql_query("SELECT * FROM empresa where id=1");
if ($dato = mysql_fetch_array($can)) {
	$empresa = $dato['empresa'];
	$nit = $dato['somos'];
	$direccion = $dato['direccion'];
	$ciudad = $dato['ciudad'];
	$tel1 = $dato['tel1'];
	$tel2 = $dato['tel2'];
	$web = $dato['web'];
	$correo = $dato['correo'];
	$region = $dato['region'];
	$eslogan = $dato['eslogan'];
}
include "include/header.php";
?>
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">

                         <table width="80%" border="0" class="table">
			<tr class="info">
				<td>
				<center>
					<strong>Actualizar Datos de la Empresa</strong>
				</center></td>
			</tr>
			<tr>
				<td>
				<form action="" method="post" enctype="multipart/form-data" name="form1">
					<table width="80%" border="0">
						<tr>
							<td width="39%"><label for="textfield">Nombre Empresa: </label>
							<input type="text" name="empresa" id="empresa" value="<?php echo $empresa; ?>" required>
							<label for="textfield">Direccion: </label>
							<input type="text" name="direccion" id="direccion" value="<?php echo $direccion; ?>">
							<label for="textfield">Celular: </label>
							<input type="text" name="celular" id="celular" value="<?php echo $tel2; ?>">
							<label for="textfield">Correo: </label>
							<input type="text" name="correo" id="correo" value="<?php echo $correo; ?>">
							<label for="">Eslogan: </label>
							<input type="text" name="eslogan" id="eslogan" value="<?php echo $eslogan; ?>">
							</td>
							<td width="32%" rowspan="2"><label for="textfield">Somos: </label>
							<input type="text" name="nit" id="nit" value="<?php echo $nit; ?>" required>
							<label for="textfield">Ciudad: </label>
							<input type="text" name="ciudad" id="ciudad" value="<?php echo $ciudad; ?>">
							<label for="textfield">Telefono: </label>
							<input type="text" name="telefono" id="telefono" value="<?php echo $tel1 ?>">
							<label for="textfield">Pagina Web: </label>
							<input type="text" name="web" id="web" value="<?php echo $web; ?>">
							<label for="textfield">Configuracion Regional</label>
							<select name="region" id="region">
								<?php
								$can=mysql_query("SELECT * FROM region");
								while($dato=mysql_fetch_array($can)){
								?>
								<option value="<?php echo $dato['id']; ?>" <?php
								if ($dato['id'] == $region) { echo 'selected';
								}
 ?> > <?php echo $dato['nombre']; ?> </option>
								<?php } ?>
							</select></td>
							<td width="29%" rowspan="2">
							<center>
								<label for="fileField">Subir Logo Empresarial</label>
							</center>
							<center><img src="img/logo.png" width="200" height="200" class="img-polaroid">
							</center>
							<br>
							<input type="file" name="imagen" id="imagen">
							</td>
						</tr>
						<tr>
							<td height="85" colspan="3">
							<button class="btn btn-large btn-primary" type="submit">
								Actualizar Datos
							</button></td>
						</tr>
						<tr>
							<td colspan="3">
							<?php
							if ($mensaje == "1") {
								echo '	<div class="alert alert-success">
<button type="button" class="close" data-dismiss="alert">X</button>
<strong>Datos de la Empresa! </strong> Actualizado con Exito
</div>';
							}
							?></td>
						</tr>
					</table>
				</form> </div>
				</div> </td>
			</tr>
		</table>

                        </div>
                    </div>
                
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
		
		
<?php

include "include/footer.php";
?>