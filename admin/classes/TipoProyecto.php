<?php
/**
 * Created by PhpStorm.
 * User: Axel
 * Date: 08/03/2015
 * Time: 05:28 PM
 */

class TipoProyecto {

    #De nuestra referencia
    public $id,
        $nombre_proyecto;
//        $author;

    # Nos permite facilmente setar atributos en la clase

    public function __construct(array $data=null){
        if(empty($data))
            return;

        foreach ($data as $field => $value){
            $this->$field = $value;
        }
    }

}

class Pais{
    #De nuestra referencia
    public $Countryid,
        $Country;
//        $author;

    # Nos permite facilmente setar atributos en la clase

    public function __construct(array $data=null){
        if(empty($data))
            return;

        foreach ($data as $field => $value){
            $this->$field = $value;
        }
    }

}

class Campamento{
    #De nuestra referencia
//:titulo,:clave,:fecha,
//:descripcion,:alojamiento,
//:localidad,:extras,:imagen_1,
//:imagen_2,:imagen_3,:imagen_4,
//:lugar,:tipo,'',''
    public $titulo,
            $clave,
            $fecha,
            $descripcion,
            $alojamiento,
            $localidad,
            $extras,
            $imagen_1,
            $imagen_2,
            $imagen_3,
            $imagen_4,
            $lugar,
            $tipo;

//        $author;

    # Nos permite facilmente setar atributos en la clase

    public function __construct(array $data=null){
        if(empty($data))
            return;

        foreach ($data as $field => $value){
            $this->$field = $value;
        }
    }
}
