<?php

include "includes/funciones.php";
//include ('admin/php_conexion.php');
$titulo = "Contacto";

include ('includes/header3.php');

//obtener testos sider
$query = "SELECT * FROM empresa";

//#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);

while ($texto = $resultado -> fetch_assoc()) {
	$empresa1 = $texto['empresa'];
	$somos = $texto['somos'];
	$direccion = $texto['direccion'];
	$filosofia = $texto['filosofia'];
	$eslogan = $texto['eslogan'];
}
?>


        <section id="ccr-left-section" class="col-md-8">

			<div class="current-page">
				<a href="index.php"><i class="fa fa-home"></i> <i class="fa fa-angle-double-right"></i></a> <?= $titulo ?>
			</div> <!-- / .current-page -->

			<section id="ccr-contact-form">
				<p>Contactanos</p>
				<form action="mail.php" method="post" id="commentform">
					<input id="author" name="author" type="text" placeholder="Nombre" value="" required>
					<input id="email" name="email" type="email" placeholder="Email" value="" required>
<!--					<input id="url" name="url" type="url" placeholder="Website" value="">-->
					<textarea id="comment" name="comment" placeholder="Meensaje" rows="8" required></textarea>
					<input name="submit" type="submit" id="submit" value="Enviar">

				</form> <!-- /#commentform -->

			</section> <!-- /#ccr-contact-form -->



		</section><!-- /.col-md-8 / #ccr-left-section -->



		<section  id="ccr-right-section" class="col-md-4">
			<section id="ccr-contact-sidebar">
					<p>
					<span><?= $titulo ?></span>
					</p>
					<address>
						<p>
							Dirección: Cuautla, Morelos
						</p>
						<p>
							México
						</p>
						<p>
							Teléfono: +52 - 17351415129
						</p>
						<p>
						Email: contact.amviac@gmail.com

						</p>
					</address>


				<p>
					<span>Redes sociales</span>
				</p>
				<ul>
<!--		 			<li>-->
<!--		 				<a href="#"  class="google-plus"><i class="fa fa-google-plus"></i></a>-->
<!--		 			</li>-->
		 			<li >
		 				<a href="https://www.facebook.com/amviac?ref=hl" class="facebook"><i class="fa fa-facebook"></i></a>
		 			</li>
					<li >
		 				<a href="https://twitter.com/AMVIAC" class="twitter"><i class="fa fa-twitter"></i></a>
		 			</li>
		 			<li >
		 				<a href="https://www.youtube.com/channel/UCMKoQjF8qpdoEUsQEvQj_8A" class="youtube"><i class="fa fa-youtube"></i></a>
		 			</li>
					
<!--		 			<li >-->
<!--		 				<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>-->
<!--		 			</li>-->
<!--		 			<li >-->
<!--		 				<a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>-->
<!--		 			</li>-->
		 		</ul>
			</section> <!-- /#ccr-contact-sidebar -->
		</section><!-- / .col-md-4  / #ccr-right-section -->


	</div><!-- /.container -->
</section><!-- / #ccr-main-section -->
<iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d15105.68900968671!2d-98.95214644999999!3d18.823883849999998!3m2!1i1024!2i768!4f13.1!2m1!1sGustavo%2BA.%2BMadero%2B50%2C%2BFrancisco%2BI.%2BMadero%2C%2B62744%2BCuautla%2C%2BMor.%2F%4018.8087474%2C-98.9568079%2C17z%2Fdata%3D*214m7*211m4*213m3*211s0x85ce6f1288558805%3A0x13774743ef1402bc*212sGustavo%2BA.%2BMadero%2B50%2C%2BFrancisco%2BI.%2BMadero%2C%2B62744%2BCuautla%2C%2BMor.*213b1*213m1*211s0x85ce6f1288558805%3A0x13774743ef1402bc!5e0!3m2!1ses!2smx!4v1429762202210" width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>


<?php
if ($titulo == "Inicio") {
	include "includes/footer3.php";
} else {
	include "includes/footer2.php";
}
?>