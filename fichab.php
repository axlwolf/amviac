<?php

session_start();
ini_set("memory_limit", "128M");

// Iniciamos el buffer
ob_start();

$tipo             = $_POST['tipo_socio'];
$tipo_pago        = $_POST['tipo_pago'];
$nombre           = $_POST['nombre'];
$apellido_paterno = $_POST['apellido_paterno'];
$apellido_materno = $_POST['apellido_materno'];
$ciudad           = $_POST['ciudad'];
$postal           = $_POST['postal'];
$fecha_nac        = $_POST['fecha_nac'];
$lugar_nac        = $_POST['lugar_nac'];
$tel_fijo         = $_POST['telefono_fijo'];
$tel_cel          = $_POST['telefono_celular'];
$nacionalidad     = $_POST['nacionalidad'];
$email            = $_POST['email'];
$profesion        = $_POST['profesion'];
$experiencia      = $_POST['experiencia'];
$enteraste        = $_POST['enteraste'];
$lugar            = $_POST['lugar'];
$fecha            = $_POST['fecha'];

// print_r($_POST);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Ficha de Membrecia AMVIAC 2015</title>
    <link rel="stylesheet" href="style.css" media="all" />
</head>
<body style="margin: 0 auto">
<div class="main">

<header class="clearfix">
    <div id="logo">
        <img src="logo-sitio.png">
    </div>
    <p style="text-align: center">
        Gustavo A. Madero Nº 50, Col. Fco. I. Madero, Cuautla, Morelos, México <br/>
        Tel. *52 735 3525181   Cel. + 52 735 1730981 <br/>
        <a href="mailto:contact.amviac@gmail.com." style="color: #9c0101">contact.amviac@gmail.com.</a>
    </p>
    <h1>Ficha de membresía 2015</h1>
    <p>
        Quiero aherir a la Asociación Mexicana de Voluntariado Internacional (AMVIAC) y me comprometo a respetar los estatutos de la misma.
    </p>

        <table class="caja">
            <thead>
            <tr>
                <td colspan="2" style="font-size: 14px; text-align: center">
                    <strong > Cuotas 2015 </strong>
                </td>
            </tr>
            </thead>
            <tr>
                <td style="font-size: 14px; text-align: center">
                    Miembro adherente: <?= $tipo; ?>
                </td>

            </tr>

        </table>
        <div id="company" class="clearfix">
            <table>
                <tr>
                    <td>
                        Banco:
                    </td>
                    <td>
                        HSBC
                    </td>
                </tr>
                <tr>
                    <td>
                        No. de cuenta:
                    </td>
                    <td>
                        6391812792
                    </td>
                </tr>
                <tr>
                    <td>
                        CLABE:
                    </td>
                    <td>
                        021542063918127928
                    </td>
                </tr>
            </table>

            <table>
                <tr>
                    <td>
                        Cuenta PayPal
                    </td>
                    <td>
                        contact.amviac@gmail.com
                    </td>
                </tr>
            </table>

        </div>
        <div id="project">
            <table class="table" style="text-align: left">
                <tr>
                    <td colspan="4">
                       Adjunto Tipo de pago:
                    </td>
                </tr>
                <tr>
                    <td>
                        <?= $tipo_pago;  ?>
                    </td>

                </tr>
            </table>
        </div>
</header>
    <table>
        <tr>
            <td>
                <label for="nombre">Nombre(s)</label>
            </td>
            <td>
                <?= $nombre;  ?>
            </td>
            <td>

             Apellido Paterno: <?= $apellido_paterno ?>
            </td>

            <td>
                Apellido Materno: <?= $apellido_materno ?>
            </td>

        </tr>
        <tr>
            <td>
                <label for="ciudad"> Ciudad</label>
            </td>
            <td>
                <?= $ciudad; ?>
            </td>
            <td>
                <label for="postal">Código postal</label>
            </td>
            <td>
                <?= $postal; ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="nacimiento">Nacimiento</label>
            </td>
            <td>
                <?= $fecha_nac ?>
            </td>
            <td>
                <label for="lugar_nac">Lugar de nacimiento</label>
            </td>
            <td>
                <?= $lugar_nac ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="telefono_fijo">
                    Teléfono fijo
                </label>
            </td>

            <td>
                <?= $tel_fijo ?>
            </td>
            <td>
                <label for="telefono_celular">
                    Teléfono celular
                </label>
            </td>

            <td>
                <?= $tel_cel ?>
            </td>
        </tr>


        <tr>
            <td>
                <label for="nacionalidad">Nacionalidad</label>
            </td>
            <td colspan="3">
                <?= $nacionalidad; ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="email">
                    Email
                </label>
            </td>
            <td colspan="3">
                <?= $email; ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="profesion">
                    Profesión
                </label>
            </td>
            <td colspan="3">
                <?= $profesion; ?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="experiencia">
                    Experiencia en el voluntariado
                </label>
            </td>
            <td colspan="3">
                <?= $experiencia;?>
            </td>
        </tr>
        <tr>
            <td>
                <label for="enteraste">¿Cómo te enteraste de AMVIAC?</label>
            </td>
            <td colspan="3">
                <?= $enteraste ?>
            </td>
        </tr>

    </table>

    <table>
        <tr>
            <td>
                <label for="lugar">
                    Lugar:                  <?= $lugar ?>

                </label>
            </td>
            <td>
                <label for="fecha">
                    Fecha:                  <?= $fecha ?>

                </label>
            </td>
            <td>

                Firma:  _________________________________________

            </td>
        </tr>

    </table>

    <!--        <input type="submit" value="Enviar datos" class="button blue large"/>-->

</div>

</body>

</html>
<?php

// exit();
include "admin/Conexion.php";


$query = "INSERT INTO miembros (tipo_socio, adjunto, nombre, apellido_paterno, apellido_materno, ciudad, codigo_postal, fecha_nacimiento, lugar_nacimiento, telefono_fijo, telefono_celular, nacionalidad, email, profesion, exp_voluntariado, enteraste, lugar, fecha_actual,estado,extra_2,extra_3)
VALUES ('$tipo', '$tipo_pago','$nombre', '$apellido_paterno','$apellido_materno','$ciudad', '$postal', '$fecha_nac', '$lugar_nac', '$tel_fijo', '$tel_cel', '$nacionalidad', '$email', '$profesion', '$experiencia', '$enteraste', '$lugar', '$fecha', '','','')
";
#Resultado
$resultado = $conexion -> query($query) or die($conexion -> error . __LINE__);


$dir = dirname(__FILE__);
require_once ("includes/dompdf/dompdf_config.inc.php");
$dompdf = new DOMPDF();
$dompdf -> load_html(ob_get_clean());
$dompdf -> render();
$pdf = $dompdf -> output();
ob_start();

require_once($dir.'/html.php');
$html_message = ob_get_contents();
ob_end_clean();


$filename = "ficha_membrecia".date('s').".pdf";

file_put_contents("pdf/".$filename, $pdf);
$dompdf -> stream($filename);

require_once($dir.'/swift/swift_required.php');

$mailer = new Swift_Mailer(new Swift_MailTransport()); // Create new instance of SwiftMailer

$message = Swift_Message::newInstance()
    ->setSubject('Ficha de Membresía AMVIAC') // Message subject
    ->setTo(array($email => $nombre, 'contac.amviac@gmail.com' => 'Contacto AMVIAC')) // Array of people to send to
    ->setFrom(array('contac.amviac@amviac.org' => 'Contacto AMVIAC')) // From:
    ->setBody($html_message, 'text/html') // Attach that HTML message from earlier
    ->attach(Swift_Attachment::newInstance($pdf, $filename, 'application/pdf')); // Attach the generated PDF from earlier

// Send the email, and show user message
if ($mailer->send($message))
    $success = true;
else
    $error = true;



?>