<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Ficha de Membrecia AMVIAC 2015</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body style="margin: 0 auto">
		<header class="clearfix">
			<div id="logo">
				<img src="logo-sitio.png">
			</div>
			<p style="text-align: center">
				Gustavo A. Madero Nº 50, Col. Fco. I. Madero, Cuautla, Morelos, México
				<br/>
				Tel. *52 735 3525181   Cel. + 52 735 1730981
				<br/>
				<a href="mailto:contact.amviac@gmail.com." style="color: #9c0101">contact.amviac@gmail.com.</a>
			</p>
			<h1>Ficha de membresía 2015</h1>
			<p>
				Quiero aherir a la Asociación Mexicana de Voluntariado Internacional (AMVIAC) y me comprometo a respetar los estatutos de la misma.
			</p>

			<form action="fichab.php" method="post">
				<table class="caja">
					<thead>
						<tr>
							<td colspan="3" style="font-size: 14px; text-align: center"><strong > Cuotas <?= date('Y') ?>
							</strong></td>
						</tr>
					</thead>
					<tr>
						<td style="font-size: 14px; text-align: center"><label for="tipo_socio">Tipo de Socio</label>
						<select name="tipo_socio" id="tipo_socio">
							<option value="Miembro Adherente">Miembro adherente $100.00 pesos (anuales)</option>
							<option value="Miembro Activo">Miembro activo $150.00 pesos (anuales) </option>
							<option value="Organización Social">Organización social $300.00 pesos (anuales)</option>
							<option value="Benefactor">Benefactor a partir de $300.00 pesos</option>
						</select></td>

					</tr>

				</table>
				<div id="company" class="clearfix">
					<table>
						<tr>
							<td> Banco: </td>
							<td> HSBC </td>
						</tr>
						<tr>
							<td> No. de cuenta: </td>
							<td> 6391812792 </td>
						</tr>
						<tr>
							<td> CLABE: </td>
							<td> 021542063918127928 </td>
						</tr>
					</table>

					<table>
						<tr>
							<td> Cuenta PayPal </td>
							<td> contact.amviac@gmail.com </td>
						</tr>
					</table>

				</div>
				<div id="project">
					<table class="table" style="text-align: left">
						<tr>
							<td colspan="4"> Adjunto </td>
						</tr>
						<tr>
							<td>
							<input type="radio" name="tipo_pago" value="Especie"/>
							</td>
							<td> la suma de </td>
							<td> ................ </td>
							<td> pesos. </td>
						</tr>
						<tr>
							<td>
							<input type="radio" name="tipo_pago" value="Cheque"/>
							</td>
							<td> un cheque de </td>
							<td> ................ </td>
							<td> pesos a la orden de AMVIAC. </td>
						</tr>
						<tr>
							<td>
							<input type="radio" name="tipo_pago" value="Deposito o Transferencia"/>
							</td>
							<td colspan="3"> copia de recibo de depósito o transferencia bancaria </td>

						</tr>
						<tr>
							<td>
							<input type="radio" name="tipo_pago" value="PayPal"/>
							</td>
							<td colspan="3"> recibo de pago por PayPal </td>

						</tr>
					</table>
				</div>
		</header>
		<main>
			<table>
				<tr>
					<td><label for="nombre">Nombre</label></td>
					<td>
					<input type="text" id="nombre" name="nombre" required/>
					</td>
					<td><label for="apellido_paterno"></label>
					<input type="text" id="apellido_paterno" name="apellido_paterno" placeholder="Apellido Paterno" required/>
					</td>
					<td><label for="apellido_materno"></label>
					<input type="text" id="apellido_materno" name="apellido_materno" placeholder="Apellido Materno" required/>
					</td>
				</tr>
				<tr>
					<td><label for="ciudad"> Ciudad</label></td>
					<td>
					<input type="text" id="ciudad" name="ciudad" required/>
					</td>
					<td><label for="postal">Código postal</label></td>
					<td>
					<input type="text" id="postal" name="postal" required/>
					</td>
				</tr>
				<tr>
					<td><label for="estado"> Estado:</label></td>
					<td colspan="3">
					<input type="text" id="estado" name="estado" required/>
					</td>
				</tr>
				<tr>
					<td><label for="fecha_nac">Nacimiento</label></td>
					<td> año / mes / día
					<br>
					<input type="text" id="fecha_nac" name="fecha_nac" placeholder="aaaa/mm/dd"/ required>
					</td>
					<td><label for="lugar_nac">Lugar de nacimiento</label></td>
					<td>
					<input type="text" id="lugar_nac" name="lugar_nac" required/>
					</td>
				</tr>
				<tr>
					<td><label for="telefono_fijo"> Teléfono fijo </label></td>

					<td>
					<input type="tel" id="telefono_fijo" name="telefono_fijo" required/>
					</td>
					<td><label for="telefono_celular"> Teléfono celular </label></td>

					<td>
					<input type="tel" id="telefono_celular" name="telefono_celular" required/>
					</td>
				</tr>

				<tr>
					<td><label for="nacionalidad">Nacionalidad</label></td>
					<td colspan="3">
					<input type="text" id="nacionalidad" name="nacionalidad"/>
					</td>
				</tr>
				<tr>
					<td><label for="email"> Email </label></td>
					<td colspan="3">
					<input type="email" id="email" name="email" required/>
					</td>
				</tr>
				<tr>
					<td><label for="profesion"> Profesión </label></td>
					<td colspan="3">
					<input type="text" id="profesion" name="profesion" required/>
					</td>
				</tr>
				<tr>
					<td><label for="experiencia"> Experiencia en el voluntariado </label></td>
					<td colspan="3"><!--                  <input type="text" id="experiencia" name="experiencia"/>
					-->
					<input type="radio" name="experiencia" id="experiencia"/>
					Si
					<input type="radio" name="experiencia" id="experiencia"/>
					No </td>
				</tr>
				<tr>
					<td>
          <label for="enteraste">¿Cómo te enteraste de AMVIAC?</label>
          </td>
					<td colspan="3"><!--                  <input type="text" id="enteraste" name="enteraste"/>
					-->
					<select name="enteraste" id="enteraste">
						<option value="FaceBook">Por Facebook</option>
						<option value="amigo o conocido">Por amigo o conocido</option>
						<option value="Google">Por Google</option>
						<option value="Otras asociaciones">Por otras asociacones</option>
						<option value="Dependencia gubernamental">Por dependencia gubernamental</option>
					</select></td>
				</tr>

			</table>

			<table>
				<tr>
					<td><label for="lugar"> Lugar:
						<input type="text" id="lugar" name="lugar"/>
					</label></td>
					<td><label for="fecha"> Fecha:
						<input type="text" id="fecha" name="fecha" value="<?=  date("Y-m-d"); ?>" readonly/>
					</label></td>
					<td> Firma:  _________________________________________ </td>
				</tr>

			</table>

			<!--        <input type="submit" value="Enviar datos" class="button blue large"/>-->
			<input class="button blue" type="submit" value="Enviar">
			</form>

		</main>

	</body>
	<script src="http://code.jquery.com/jquery.js"></script>
	<script>
		$('.button').mousedown(function(e) {
			var target = e.target;
			var rect = target.getBoundingClientRect();
			var ripple = target.querySelector('.ripple');
			$(ripple).remove();
			ripple = document.createElement('span');
			ripple.className = 'ripple';
			ripple.style.height = ripple.style.width = Math.max(rect.width, rect.height) + 'px';
			target.appendChild(ripple);
			var top = e.pageY - rect.top - ripple.offsetHeight / 2 - document.body.scrollTop;
			var left = e.pageX - rect.left - ripple.offsetWidth / 2 - document.body.scrollLeft;
			ripple.style.top = top + 'px';
			ripple.style.left = left + 'px';
			return false;
		});
	</script>
</html>