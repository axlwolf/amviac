<?php

include "includes/funciones.php";
$titulo = "Inicio";

include ('includes/header3.php');

include "admin/include/ConexionPDO.php";
include "admin/classes/TipoProyecto.php";

# Seleccionamos los tipos de proyectos
$tipo_proyeco = $dbh -> query("SELECT id,nombre_proyecto FROM tipo_proyecto");
$tipos = $tipo_proyeco -> fetchAll(PDO::FETCH_CLASS, 'TipoProyecto');

# Seleccionamos los paises
$paises = $dbh -> query("SELECT Countryid,Country FROM paises_carga ORDER BY Country ASC");
$select_pais = $paises -> fetchAll(PDO::FETCH_CLASS, 'Pais');
?>
<!-- icono en el navegador -->
<link rel="stylesheet" type="text/css" href="css/stilosAbd.css" />
<link rel="stylesheet" href="css/reveal.css">
<script type="text/javascript" src="js/scriptAbd.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
	<script type="text/javascript" src="js/jquery.reveal1.js"></script>
	<link rel="stylesheet" type="text/css" href="css/stylAbd.css" />

		<div class="row">
			<section id="ccr-left-section" class="col-md-9">

				<div class="ccr-last-update">
					<div class="update-ribon"><strong><?= $titulos[1]; ?></div> <!-- /.update-ribon -->
					<span class="update-ribon-right"></span> <!-- /.update-ribon-left -->
					<div class="update-news-text" id="update-news-text">
						
						<!-- <a href="ficha_inscripcion.php" class="btn btn-primary">Ficha de Inscripción</a> -->
						<a href="ficha-espanish.doc" class="btn btn-primary">Ficha de Inscripción - Español</a>
						<a href="ficha-ingles.doc" class="btn btn-primary">Ficha de Inscripción - English</a>
					</div>

					<div class="update-right-border"></div> <!-- /.update-right-border -->
				</div> <!-- / .ccr-last-update -->

<div id="myModal" class="reveal-modal"></div>
				<section id="ccr-slide-main" class="carousel slide" data-ride="carousel">

						<!-- Carousel items -->
						<div class="carousel-inner">

							<div class="active item">
								<div class="container slide-element">
									<img src="images-slider/8.jpg" alt="Main Slide Image 1">
									<p><a href="#">Convivencia de Voluntariados <?php /*$texto_s[1]*/ ?></a></p>
								</div> <!-- /.slide-element -->
							</div> <!--/.active /.item -->

							<div class="item">
								<div class="container slide-element">
									<img src="images-slider/24.jpg" alt="Main Slide Image 2">
									<p><a href="#">Voluntariado<?php /*$texto_s[2]*/ ?></a></p>
								</div> <!-- /.slide-element -->
							</div> <!--  /.item -->

							<div class="item">
								<div class="container slide-element">
									<img src="images-slider/slide4.png" alt="Main Slide Image 3">
									<p><a href="#">Voluntariado<?php /*$texto_s[3]*/ ?></a></p>
								</div> <!-- /.slide-element -->
							</div> <!-- .item -->

							<div class="item">
								<div class="container slide-element">
									<img src="images-slider/slide2.png" alt="Main Slide Image 4">
									<p><a href="#">Ayudando</a></p>
								</div> <!-- /.slide-element -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="container slide-element">
									<img src="images-slider/slide1.png" alt="Main Slide Image 5">
									<p><a href="#">Voluntariado</a></p>
								</div> <!-- /.slide-element -->
							</div> <!-- /.item -->
							<div class="item">
								<div class="container slide-element">
									<img src="images-slider/14.jpg" alt="Main Slide Image 6">
									<p><a href="#">Voluntariados en Acción</a></p>
								</div> <!-- /.slide-element -->
							</div> <!-- /.item -->

						</div> <!-- /.carousel-inner -->

						<!-- slider nav -->
						<a class="carousel-control left" href="#ccr-slide-main" data-slide="prev"><i class="fa fa-arrow-left"></i></a>
						<a class="carousel-control right" href="#ccr-slide-main" data-slide="next"><i class="fa fa-arrow-right"></i></a>

						<ol class="carousel-indicators">
							<li data-target="#ccr-slide-main" data-slide-to="0" class="active"></li>
							<li data-target="#ccr-slide-main" data-slide-to="1"></li>
							<li data-target="#ccr-slide-main" data-slide-to="2"></li>
							<li data-target="#ccr-slide-main" data-slide-to="3"></li>
						</ol> <!-- /.carousel-indicators -->


				</section><!-- /#ccr-slide-main -->

			</section><!-- /.col-md-8 / #ccr-left-section -->

			<aside id="ccr-right-section" class="col-md-3 ccr-home">
				<section id="eventos">
					<br/><center><h4>Próximos Eventos</h4></center>
					<article id="uno" class="big-link" data-reveal-id="myModal"><img src="img-actividades/ecuador1.jpg"><span>*Voluntariado en Ecuador 2016</span></article>
					<article id="dos" class="big-link" data-reveal-id="myModal"><img src="img-actividades/india1.jpg"><span>*Voluntariado en la India 2016</span></article>
					<article id="tres" class="big-link" data-reveal-id="myModal"><img src="img-actividades/JAPON2.jpg"><span>*Voluntariado en Japón 2016</span></article>
					<!-- <article id="dos" class="big-link" data-reveal-id="myModal"><img src="img-actividades/9.jpg"><span>*Voluntariado Internacional Francia</span></article>
					<article id="tres" class="big-link" data-reveal-id="myModal"><img src="img-actividades/3.png"><span>*Voluntario Internacional 2015</span></article>
					<article id="cuatro" class="big-link" data-reveal-id="myModal"><img src="img-actividades/4.jpg"><span>*Voluntarios del Patrimonio Mundial</span></article> -->
					<br/><center><h4>Programas</h4></center>
					<article class="programas"><a href="programas/006/AMVIAC1.pdf" target="_blank"><img src="programas/006/AMVIAC1.jpg"><span>*Programa Campamentos Español</span></a></article>
					<article class="programas"><a href="programas/006/AMVIAC2.pdf" target="_blank"><img src="programas/006/AMVIAC2.jpg"><span>*Programa Campamentos English</span></a></article>
				</section>
				<section id="ccr-find-on-fb">
					
					<!--<div class="find-fb-title">
						<span><i class="fa fa-facebook"></i></span> Encuentranos en facebook
					</div> /.find-fb-title -->
					<!--<div class="find-on-fb-body">

						<<div class="fb-like-box" data-href="https://www.facebook.com/amviac/" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false" style="width: 80%"></div>

					</div> /.find-on-fb-body -->
				</section> <!-- /#ccr-find-on-fb -->

				<section id="sidebar-popular-post" style="margin-bottom: 3em">
					<div class="ccr-gallery-ttile">
						<span></span>
						<p><strong>Buscador de proyectos</strong></p>


					</div> <!-- .ccr-gallery-ttile -->

					<!-- <form action="#" style="margin-top: 2em; text-align:center"> -->
					<div style="margin-top: 2em; text-align:center">
						<fieldset>
							
<legend><button class="btn btn-success btn-lg" onClick="verProyectos();" style="font-size: smaller;">Ver todos los proyectos</button>
</legend>
							<div class="form-group">
								<label for="duracion">
									Duración del proyecto

									<select name="duracion" id="duracion">
										<option value="corto">Corto plazo (2 o 3 semanas)</option>
										<option value="mediano">Mediano (2 a 5 meses)</option>
										<option value="largo">Largo plazo (6 a 12 meses)</option>
									</select>
									
								</label>
							</div>
							<div class="form-group">
								<label for="Tipo"> Tipo de proyecto
									<select name="tipo" id="tipo" class="form-control">
										<?php
										foreach($tipos as $tipo):

										?>
												<option value="<?= $tipo -> id ?>"><?= $tipo -> nombre_proyecto ?></option>

										<?php
										endforeach;
										?>
									</select>
								</label>
							</div>
							<div class="form-group">
								<label for="Lugar">Lugar del proyecto
									<select name="lugar" id="lugar">
										<?php
										foreach($select_pais as $pais):
										?>
											<option  value="<?= $pais -> Countryid ?>"><?= $pais -> Country ?></option>
										<?php
										endforeach;
										?>
									</select>
								</label>
							</div>
						</fieldset>
						<!-- <input type="submit" class="btn btn-success btn-lg" value="Buscar" /> -->
						<button class="btn btn-success btn-lg" onClick="buscador();">Buscar</button>
					</div>
					<section id="icons-abd">
						<br/>
				<p>
					<span>Redes sociales</span>
				</p>
		 			<nav id="fac"><a href="https://www.facebook.com/amviac?ref=hl" class="facebook"><i class="fa fa-facebook"></i></a></nav>
		 			<nav id="twi"><a href="https://twitter.com/AMVIAC" class="twitter"><i class="fa fa-twitter"></i></a></nav>
			</section> <!-- /#ccr-contact-sidebar -->
					<!-- </form> -->

				</section> <!-- /#sidebar-popular-post -->
			</aside><!-- / .col-md-4  / #ccr-right-section -->
		</div>
		<div class="row">
			<section>
				<div class="callout-content">
					<div class="<?= $titulo_cuadro[1]; ?>" style="margin-top:-10.5%;">
						<h1>	<strong style="margin-left:17%;"><?= $titulos[1]; ?></strong></h1>
						<div style="width:70%; font-size:18px; margin-left:3%; font-weight: normal;" class="well" align="justify">
						La Asociación Mexicana de Voluntariado Internacional A.C. es una organización de la sociedad civil nacida a iniciativa de ex-voluntarios mexicanos y profesionistas deseosos de participar en la transformación de país a través de la integración del Voluntariado Internacional en proyectos organizados en México por asociaciones civiles, ayuntamientos, comunidades y grupos organizados, así como con el envío de mexicanos a proyectos en el extranjero con la idea de que con esta experiencia puedan, al regreso al país, involucrarse de una forma más activa en el quehacer de la comunidad.
						</div>
					</div>
				</div>
			</section>
		</div>



<?php

include "includes/footer2.php";
?>