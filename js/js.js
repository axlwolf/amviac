var pictures = new Array('img-actividades/actividades/1.jpg','img-actividades/actividades/2.jpg','img-actividades/actividades/3.jpg','img-actividades/actividades/4.jpg','img-actividades/actividades/5.jpg','img-actividades/actividades/6.jpg','img-actividades/actividades/7.jpg');
var functions = new Array('func1()','func2()','func3()','func4()','func5()','func6()');
var items = pictures.length;
var angles = new Array(items);
var images = new Array(items);
var rx = 250;
var ry = 70;
var cx = 0;
var cy = 0;

function itemSize() {
	
	for (var i = 0; i < items; i++) {
		images[i] = new Image();
		images[i].src = pictures[i];
	}
	initCar()
}
function  initCar() {

	var t=1;
	var content = document.getElementById('content');
	for (var i = 0; i < items; i++) {
		angles[i] = ((Math.PI * 2) / items) * i;
		var xpos = (Math.cos(angles[i]) * rx) + cx;
		var ypos = (Math.sin(angles[i]) * ry) + cy;
		var obj = newObj(t,i, xpos, ypos, parseInt(ypos), pictures[i]);
		content.innerHTML += obj;
		t=t+1;
	}
	setInterval('rotateCar()', 50);
}

function rotateCar() {

	for (var i = 0; i < items; i++) {
		angles[i] += 0.008;
		var xpos = (Math.cos(angles[i]) * rx) + cx;
		var ypos = (Math.sin(angles[i]) * ry) + cy;
		var obj = document.getElementById('obj' + i);
		obj.style.left = xpos + 'px';
		obj.style.top = ypos + 'px';
		obj.style.zIndex = parseInt(ypos);

		var objImg = document.getElementById('img' + i);
		var delta = (ypos - cy + ry) / (2 * ry);
		delta = (delta + 3) / 4;
		objImg.style.height = (delta * images[i].height) + 'px';
		objImg.style.width = (delta * images[i].width) + 'px';
		
	}
}

function newObj(t,id,x,y,z,src) {
	return '<div id="obj'+ id + '" onClick="clickItem('+ id +')" style="position:absolute; left:' + x + 'px; top:'+ y + 'px; z-index:'+ z +'; width:100px;"><a href="#'+id+'"><img id="img'+ id + '" src="'+ src + '" class="imgs-act" class="big-link" data-reveal-id="myModal"/>Actividad-'+t+'</a></div>';
}

/*function clickItem(id) {
	//eval(functions[id]);
	jAlert('This is a custom alert box', 'Alert Dialog');
	$("img#img"+id).css({"border":"2px solid red"});

}
*/

/*function func1() {
	alert('Item 1');
}
function func2() {
	alert('Item 2');
}
function func3() {
	alert('Item 3');
}
function func4() {
	alert('Item 4');
}
function func5() {
	alert('Item 5');
}
function func6() {
	alert('Item 6');
}*/