/*
 * jQuery Reveal Plugin 1.0
 * www.ZURB.com
 * Copyright 2010, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
*/

(function($) {

/*---------------------------
 Defaults for Reveal
----------------------------*/
	 
/*---------------------------
 Listener for data-reveal-id attributes
----------------------------*/

	$('article[data-reveal-id]').live('click', function(e) {
		e.preventDefault();

	/*variables mias*/		
	var idImage=$(this).attr('id');
	if (idImage=='uno') {
		var html="<img src='img-actividades/ecuador1.jpg'>";
	}else if (idImage=='dos') {
		var html="<img src='img-actividades/india1.jpg'>";
	}else if (idImage=='tres') {
		var html="<img src='img-actividades/JAPON2.jpg'>";
	}else if (idImage=='cuatro') {
		var html="<img src='img-actividades/9.jpg'>";
	}else if (idImage=='cinto') {
		var html="<img src='img-actividades/3.png'>";
	}else if (idImage=='seis') {
		var html="<a href='http://whc.unesco.org/en/whvolunteers/'  target='_blank'><img src='img-actividades/4.jpg'></a>";
	}
	//html +='<a class="close-reveal-modal">&#215;</a>';
	$("div#myModal").html(html)
	/*hasta aqui terminan*/
	var modalLocation = $(this).attr('data-reveal-id');
	$('#'+modalLocation).reveal($(this).data());
	});

//segunda funcion
	$('img[data-reveal-id]').live('click', function(e) {
		e.preventDefault();

	/*variables mias*/		
	var idImage=$(this).attr('id');

	if (idImage=='img0') {
		var html="<img src='img-actividades/actividades/actividades/1.jpg'>";
	}else if (idImage=='img1') {
		var html="<img src='img-actividades/actividades/actividades/2.jpg'>";
	}else if (idImage=='img2') {
		var html="<img src='img-actividades/actividades/actividades/3.jpg'>";
	}else if (idImage=='img3') {
		var html="<img src='img-actividades/actividades/actividades/4.jpg'>";
	}else if (idImage=='img4') {
		var html="<img src='img-actividades/actividades/actividades/5.jpg'>";
	}else if (idImage=='img5') {
		var html="<img src='img-actividades/actividades/actividades/6.jpg'>";
	}else if (idImage=='img6') {
		var html="<img src='img-actividades/actividades/actividades/7.jpg'>";
	}
	
	//html +='<a class="close-reveal-modal">&#215;</a>';
	$("div#myModal").html(html)
	/*hasta aqui terminan*/
	var modalLocation = $(this).attr('data-reveal-id');
	$('#'+modalLocation).reveal($(this).data());
	});

/*---------------------------
 Extend and Execute
----------------------------*/

    $.fn.reveal = function(options) {
        
        
        var defaults = {  
	    	animation: 'fadeAndPop', //fade, fadeAndPop, none
		    animationspeed: 300, //how fast animtions are
		    closeonbackgroundclick: true, //if you click background will modal close?
		    dismissmodalclass: 'close-reveal-modal' //the class of a button or element that will close an open modal
    	}; 
    	
        //Extend dem' options
        var options = $.extend({}, defaults, options); 
	
        return this.each(function() {
        
/*---------------------------
 Global Variables
----------------------------*/
        	var modal = $(this),
        		topMeasure  = parseInt(modal.css('top')),
				topOffset = modal.height() + topMeasure,
          		locked = false,
				modalBG = $('.reveal-modal-bg');

/*---------------------------
 Create Modal BG
----------------------------*/
			if(modalBG.length == 0) {
				modalBG = $('<div class="reveal-modal-bg" />').insertAfter(modal);
			}		    
     
/*---------------------------
 Open & Close Animations
----------------------------*/
			//Entrance Animations
			modal.bind('reveal:open', function () {
			  modalBG.unbind('click.modalEvent');
				$('.' + options.dismissmodalclass).unbind('click.modalEvent');
				if(!locked) {
					lockModal();
					if(options.animation == "fadeAndPop") {
						modal.css({'top': $(document).scrollTop()-topOffset, 'opacity' : 0, 'visibility' : 'visible'});
						modalBG.fadeIn(options.animationspeed/2);
						modal.delay(options.animationspeed/2).animate({
							"top": $(document).scrollTop()+topMeasure + 'px',
							"opacity" : 1
						}, options.animationspeed,unlockModal());					
					}
					if(options.animation == "fade") {
						modal.css({'opacity' : 0, 'visibility' : 'visible', 'top': $(document).scrollTop()+topMeasure});
						modalBG.fadeIn(options.animationspeed/2);
						modal.delay(options.animationspeed/2).animate({
							"opacity" : 1
						}, options.animationspeed,unlockModal());					
					} 
					if(options.animation == "none") {
						modal.css({'visibility' : 'visible', 'top':$(document).scrollTop()+topMeasure});
						modalBG.css({"display":"block"});	
						unlockModal()				
					}
				}
				modal.unbind('reveal:open');
			}); 	

			//Closing Animation
			modal.bind('reveal:close', function () {
			  if(!locked) {
					lockModal();
					if(options.animation == "fadeAndPop") {
						modalBG.delay(options.animationspeed).fadeOut(options.animationspeed);
						modal.animate({
							"top":  $(document).scrollTop()-topOffset + 'px',
							"opacity" : 0
						}, options.animationspeed/2, function() {
							modal.css({'top':topMeasure, 'opacity' : 1, 'visibility' : 'hidden'});
							unlockModal();
						});					
					}  	
					if(options.animation == "fade") {
						modalBG.delay(options.animationspeed).fadeOut(options.animationspeed);
						modal.animate({
							"opacity" : 0
						}, options.animationspeed, function() {
							modal.css({'opacity' : 1, 'visibility' : 'hidden', 'top' : topMeasure});
							unlockModal();
						});					
					}  	
					if(options.animation == "none") {
						modal.css({'visibility' : 'hidden', 'top' : topMeasure});
						modalBG.css({'display' : 'none'});	
					}		
				}
				modal.unbind('reveal:close');
			});     
   	
/*---------------------------
 Open and add Closing Listeners
----------------------------*/
        	//Open Modal Immediately
    	modal.trigger('reveal:open')
			
			//Close Modal Listeners
			var closeButton = $('.' + options.dismissmodalclass).bind('click.modalEvent', function () {
			  modal.trigger('reveal:close')
			});
			
			if(options.closeonbackgroundclick) {
				modalBG.css({"cursor":"pointer"})
				modalBG.bind('click.modalEvent', function () {
				  modal.trigger('reveal:close')
				});
			}
			$('body').keyup(function(e) {
        		if(e.which===27){ modal.trigger('reveal:close'); } // 27 is the keycode for the Escape key
			});
			
			
/*---------------------------
 Animations Locks
----------------------------*/
			function unlockModal() { 
				locked = false;
			}
			function lockModal() {
				locked = true;
			}	
			
        });//each call
    }//orbit plugin call
})(jQuery);
        
