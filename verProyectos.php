<?php

include "includes/funciones.php";
$titulo = "Todo los Proyectos";
include ('includes/header3.php');
include "admin/include/ConexionPDO.php";

$consulta = $dbh -> prepare("SELECT * FROM campamentos,paises_carga,tipo_proyecto 
		WHERE campamentos.id_lugar=paises_carga.CountryId AND campamentos.id_tipo_proyecto=tipo_proyecto.id AND campamentos.estado='s'");
$consulta -> execute();
	?>

	<link rel="stylesheet" href="css/reveal.css">
	<link rel="stylesheet" type="text/css" href="css/buscador.css" />
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
	<script type="text/javascript" src="js/jquery.reveal.js"></script>
	<section id="ccr-left-section" class="col-md-12">
	<?php
	if ($consulta->rowCount()>0) {
		?>
		<div class="current-page">
		<a href="index.php"><i class="fa fa-home"></i> <i class="fa fa-angle-double-right"></i></a> <?= $titulo ?>
	</div>
	<!-- / .current-page -->

	<!-- BreadCrumb -->
	<div class="body-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12" style="margin-bottom: 300px">
		<table class="table buscador">
			<th>Proyecto</th>
			<th>Clave</th>
			<th>Lugar</th>
			<th>Tipo de Proyecto</th>
			<th>Duración</th>
	<?php
	while ($rows=$consulta->fetch()) {
		?>
		<tr>
			<td class="name"><a href="#" id="<?php echo $rows['id_camp']; ?>" class="big-link" data-reveal-id="myModal"><?php echo $rows['titulo_proyecto']; ?></a></td>
			<td><?php echo $rows['clave_proyecto']; ?></td>
			<td><?php echo $rows['Country']; ?></td>
			<td><?php echo $rows['nombre_proyecto']; ?></td>
			<td><?php echo $rows['fecha'] . ' - ' . $rows['fecha2']; ?> <br /> <?php echo $rows['duracion']  ?></td>
		</tr>
		<?php
		}
	?>

	</table>
	<div id="myModal" class="reveal-modal"></div>
	</div>
			</div>
		</div>
	</div>

		<?php
		}else{
		echo "<br>No Existen Proyectos!!!";
		}
	?>
</section>
	<?php

	if ($titulo == "Inicio") {
		include "includes/footer3.php";
	} else {
		include "includes/footer2.php";
	}
?>

<script type='text/javascript' src='admin/js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>

<script type="text/javascript" src="admin/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="admin/js/plugins/bootstrap/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="admin/js/plugins/bootstrap/bootstrap-select.js"></script>